//
//  MainCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol TabItemCoordinatorProtocol: CoordinatorProtocol {
    init(coordinator: MainCoordinatorProtocol, navigationController: TabBarNarvigationController)
}

protocol MainCoordinatorProtocol: CoordinatorProtocol {
    func userLoggOut()
}

final class MainCoordinator: MainCoordinatorProtocol {
    private let parentCoordinator: ApplicationParentCordinatorProtocol
    private var tabBarController: TabBarController!
    
    private var coordinators: [CoordinatorProtocol] = []
    
    init(parentCoordinator: ApplicationParentCordinatorProtocol) {
        self.parentCoordinator = parentCoordinator
    }

    func userLoggOut() {
        parentCoordinator.presentLoginView()
    }
    
    func start() {
        let viewModel = TabBarViewModel(coordinator: self)
        tabBarController = TabBarController(viewModel: viewModel)
        
        tabBarController.viewControllers = TabBarItem.allCases.map {
            let navigationController = TabBarNarvigationController(item: $0)
            let coordinator = createTabBarCoordinator($0, navigationController: navigationController)
            coordinators.append(coordinator)
            coordinator.start()
            return navigationController
        }
        parentCoordinator.showRootViewController(rootViewController: tabBarController)
    }
    
    private func createTabBarCoordinator(_ item: TabBarItem, navigationController: TabBarNarvigationController) -> TabItemCoordinatorProtocol {
        switch item {
        case .feed:
            return FeedCoordinator(coordinator: self, navigationController: navigationController)
        case .search:
            return SearchCoordinator(coordinator: self, navigationController: navigationController)
        case .imageSelector:
            return ImageCoordinator(coordinator: self, navigationController: navigationController)
        case .notification:
            return NotificationCoordinator(coordinator: self, navigationController: navigationController)
        case .profile:
            return ProfileCoordinator(coordinator: self, navigationController: navigationController)
        }
    }
}
