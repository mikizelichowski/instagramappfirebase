//
//  LoginCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol SignInWithAppleFlowCoordinatorProtocol {
    var viewController: UIViewController { get }
}

protocol LoginCoordinatorProtocol: CoordinatorProtocol {
    func showLoginView()
    func showResetPassword()
    func showMainView()
    func showSignUp()
    func popToLoginViewController()
}

final class LoginCoordinator: LoginCoordinatorProtocol {
    private let parentCoordinator: ApplicationParentCordinatorProtocol
    private let navigationController = LoginNavigtionController()
    
    init(parentCoordinator: ApplicationParentCordinatorProtocol) {
        self.parentCoordinator = parentCoordinator
    }
    
    func showLoginView() {
//        let viewModel = LoginViewModel(coordinator: self)
//        let viewController = LoginViewController(with: viewModel)
//        navigationController.pushViewController(viewController, animated: true)
    }
    
    func showResetPassword() {
    }
    
    func showSignUp() {
//        let viewModel = RegistrationViewModel(coordinator: self)
//        let controller = RegistrationViewController(with: viewModel)
//        navigationController.pushViewController(controller, animated: true)
    }
    
    func showMainView() {
        parentCoordinator.presentMainView()
    }
    
    func popToLoginViewController() {
//        guard let mainViewController = navigationController.viewControllers.first(where: {$0 is FeedViewController}) else {
//            navigationController.popToRootViewController(animated: true)
//            return
//        }
//        let viewModel = LoginViewModel(coordinator: self)
//        let viewController = LoginViewController(with: viewModel)
//        navigationController.setViewControllers([mainViewController, viewController], animated: true)
    }
    
    func start() {
//        let viewModel = LoginViewModel(coordinator: self)
//        let viewController = LoginViewController(with: viewModel)
//        navigationController.pushViewController(viewController, animated: true)
//        parentCoordinator.showRootViewController(rootViewController: navigationController)
    }
}
