//
//  SearchCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol SearchCoordinatorProtocol: TabItemCoordinatorProtocol {
    func showProfileViewController(_ profileModelProtocol: ProfileViewModelProtocol)
}

final class SearchCoordinator: SearchCoordinatorProtocol {
    private let parentCoordinator: MainCoordinatorProtocol
    private let navigationController: TabBarNarvigationController
    
    private var user: User?
    
    init(coordinator: MainCoordinatorProtocol, navigationController: TabBarNarvigationController) {
        self.parentCoordinator = coordinator
        self.navigationController = navigationController
    }
    
    func start() {
//        let searchViewModel = SearchViewModel(coordinator: self)
//        let viewController = SearchViewController(with: searchViewModel)
//        navigationController.pushViewController(viewController, animated: true)
    }
    
    func showProfileViewController(_ profileModelProtocol: ProfileViewModelProtocol)  {
//        let viewModel = ProfileViewModel(coordinate:  profileModelProtocol)
//        let controller = ProfileViewController(with: viewModel)
//        navigationController.pushViewController(controller, animated: true)
    }
}
