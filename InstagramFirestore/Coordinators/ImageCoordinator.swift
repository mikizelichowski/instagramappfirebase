//
//  ImageCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol ImageCoordinatorProtocol: TabItemCoordinatorProtocol {
    
}

final class ImageCoordinator: ImageCoordinatorProtocol {
    private let parentCoordinator: MainCoordinatorProtocol
    private let navigationController: TabBarNarvigationController
    
    init(coordinator: MainCoordinatorProtocol, navigationController: TabBarNarvigationController) {
        self.parentCoordinator = coordinator
        self.navigationController = navigationController
    }
    
    func start() {
        let layout = UICollectionViewFlowLayout()
        let controller = VideoViewController(collectionViewLayout: layout)
        navigationController.pushViewController(controller, animated: true)
        //        let feedViewModel = SearchViewModel()
        //        let viewController = SearchViewController(with: feedViewModel)
        //        navigationController.pushViewController(viewController, animated: true)
    }

    
}
