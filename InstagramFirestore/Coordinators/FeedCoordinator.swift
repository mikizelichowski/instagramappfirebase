//
//  FeedCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol FeedCoordinatorProtocol: TabItemCoordinatorProtocol {
    func logout()
}

final class FeedCoordinator: FeedCoordinatorProtocol {
    private let parentCoordinator: MainCoordinatorProtocol
    private let navigationController: TabBarNarvigationController
    
    init(coordinator: MainCoordinatorProtocol, navigationController: TabBarNarvigationController) {
        self.parentCoordinator = coordinator
        self.navigationController = navigationController
    }
    
    func start(){
//        let feedViewModel = FeedViewModel(coordinate: self)
//        let viewController = FeedViewController(with: feedViewModel)
//        navigationController.pushViewController(viewController, animated: true)
    }
    
    func logout() {
        parentCoordinator.userLoggOut()
    }
}
