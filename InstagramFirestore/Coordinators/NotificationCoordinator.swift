//
//  NotificationCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol NotificationCoordinatorProtocol: TabItemCoordinatorProtocol {
    
}

final class NotificationCoordinator: NotificationCoordinatorProtocol {
    private let parentCoordinator: MainCoordinatorProtocol
    private let navigationController: TabBarNarvigationController
    
    init(coordinator: MainCoordinatorProtocol, navigationController: TabBarNarvigationController) {
        self.parentCoordinator = coordinator
        self.navigationController = navigationController
    }
    
    func start() {
//        let feedViewModel = SearchViewModel()
//        let viewController = SearchViewController(with: feedViewModel)
//        navigationController.pushViewController(viewController, animated: true)
    }
}
