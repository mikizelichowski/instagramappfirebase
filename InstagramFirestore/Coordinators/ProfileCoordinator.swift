//
//  ProfileCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol ProfileCoordinatorProtocol: TabItemCoordinatorProtocol {
}

final class ProfileCoordinator: ProfileCoordinatorProtocol {
    private let parentCoordinator: MainCoordinatorProtocol
    private let navigationController: TabBarNarvigationController
        
    init(coordinator: MainCoordinatorProtocol, navigationController: TabBarNarvigationController) {
        self.parentCoordinator = coordinator
        self.navigationController = navigationController
    }
    
    func start() {
//        let profileViewModel = ProfileViewModel(coordinate: self)
//        let viewController = ProfileViewController(with: profileViewModel)
//        navigationController.pushViewController(viewController, animated: true)
    }

}
