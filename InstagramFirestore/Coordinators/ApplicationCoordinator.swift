//
//  ApplicationCoordinator.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase

protocol CoordinatorProtocol: class {
    func start()
}

protocol ApplicationCoordinatorProtocol: CoordinatorProtocol {
    init(window: UIWindow?)
}

protocol ApplicationParentCordinatorProtocol {
    func showRootViewController(rootViewController: UIViewController)
    func presentLoginView()
    func presentMainView()
}

final class ApplicationCoordinator: ApplicationCoordinatorProtocol {
    private weak var window: UIWindow?
    
    private var loginCoordinator: LoginCoordinator?
    private var mainCoordinator: MainCoordinatorProtocol?
    
    required init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        if Auth.auth().currentUser?.uid == nil {
            DispatchQueue.main.async {
                self.presentLoginView()
            }
        } else {
            self.presentMainView()
        }
    }
}

extension ApplicationCoordinator: ApplicationParentCordinatorProtocol {
    func showRootViewController(rootViewController: UIViewController) {
        window?.rootViewController = rootViewController
    }
    
    func presentLoginView() {
        loginCoordinator = LoginCoordinator(parentCoordinator: self)
        loginCoordinator?.start()
    }
    
    func presentMainView() {
        mainCoordinator = MainCoordinator(parentCoordinator: self)
        mainCoordinator?.start()
    }
}
