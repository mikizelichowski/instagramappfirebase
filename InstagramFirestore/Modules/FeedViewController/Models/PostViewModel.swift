//
//  PostViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

struct PostViewModel {
    let post: Post
    
    var imageUrl: URL? { return URL(string: post.imageUrl) }
    var caption: String { return post.caption }
    var likes: Int { return post.likes }
    var userProfileImageUrl: URL? { return URL(string: post.ownerUserImageUrl) }
    var username: String { return post.ownerUsername }
    
    var likesLabelText: String { post.likes != 1 ? "\(post.likes) likes" : "\(post.likes) like"}
    
    init(post: Post) {
        self.post = post
    }
}
