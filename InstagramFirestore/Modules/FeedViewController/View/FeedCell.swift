//
//  FeedCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class FeedCell: UICollectionViewCell {
    private enum Constants {
        static let padding: CGFloat = 12.0
        static let stackHeight: CGFloat = 50.0
        static let stackWidth: CGFloat = 120.0
    }
    
    var didLikeTappedClosure: ((Post) -> ())?
    var commentTappedClosure: ((Post) -> ())?
    var usernameTappedClosure: ((Post) -> ())?
    
    var postCell: Post? {
        didSet { likesLabel.text = postCell?.likesLabelText }
    }
    
    private lazy var profileImageView = CustomUIImage(style: .smallProfile)
    private let postImageView = CustomUIImage(style: .post)
    private lazy var usernameButton = CustomButton(style: .usernameButton)
    private var likesLabel = CustomLabel(style: .lightLabel)
    private let captionLabel = CustomLabel(style: .postLabel)
    private let postTimeLabel = CustomLabel(style: .lightLabel)
    lazy var likeButton = CustomButton(style: .postButton, image: Asset.like_unselected.image)
    private lazy var commentButton = CustomButton(style: .postButton, image: Asset.comment.image)
    private lazy var shareButton = CustomButton(style: .postButton, image: Asset.send.image)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        actionButtons()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        [profileImageView,usernameButton, postImageView, likesLabel, captionLabel, postTimeLabel].forEach{ addSubview($0)}
        profileImageView.anchor(top: topAnchor,
                                left: leftAnchor,
                                paddingTop: Constants.padding,
                                paddingLeft: Constants.padding)
        usernameButton.centerY(inView: profileImageView,
                               leftAnchor: profileImageView.rightAnchor,
                               paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
        postImageView.anchor(top: profileImageView.bottomAnchor,
                             left: leftAnchor,
                             right: rightAnchor,
                             paddingTop: CGFloat(StringRepresentationOfDigit.eight))
        postImageView.heightAnchor.constraint(equalTo: widthAnchor, multiplier: CGFloat(StringRepresentationOfDigit.one)).isActive = true
        configureActionButtons()
        likesLabel.anchor(top: likeButton.bottomAnchor,
                          left: leftAnchor,
                          paddingTop: CGFloat(StringRepresentationOfDigit.four),
                          paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
        captionLabel.anchor(top: likesLabel.bottomAnchor,
                            left: leftAnchor,
                            paddingTop: CGFloat(StringRepresentationOfDigit.eight),
                            paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
        postTimeLabel.anchor(top: captionLabel.bottomAnchor,
                             left: leftAnchor,
                             paddingTop: CGFloat(StringRepresentationOfDigit.eight),
                             paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
    }
    
    private func actionButtons() {
        usernameButton.addTarget(self, action: #selector(didTapShowUserProfile), for: .touchUpInside)
        commentButton.addTarget(self, action: #selector(didTapComments), for: .touchUpInside)
        likeButton.addTarget(self, action: #selector(didTapLike), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapShowUserProfile))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(tap)
    }
    
    private func configureActionButtons() {
        let stackView = UIStackView(arrangedSubviews: [likeButton, commentButton, shareButton])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = CGFloat(StringRepresentationOfDigit.twenty)
        addSubview(stackView)
        stackView.anchor(top: postImageView.bottomAnchor, left: leftAnchor, paddingTop: CGFloat(StringRepresentationOfDigit.eight), paddingLeft: CGFloat(StringRepresentationOfDigit.eight), width: Constants.stackWidth, height: Constants.stackHeight)
    }
    
    func populate(with post: Post) {
        self.postCell = post
        guard let imageUrl = URL(string: post.imageUrl) else { return }
        guard let ownerUserImageUrl = URL(string: post.ownerUserImageUrl) else { return }
        profileImageView.sd_setImage(with: ownerUserImageUrl)
        postImageView.sd_setImage(with: imageUrl)
        captionLabel.text = post.caption
        usernameButton.setTitle(post.ownerUsername, for: .normal)
        likeButton.tintColor = post.likeButtonTintColor
        likeButton.setImage(post.likeButtonImage, for: .normal)
        postTimeLabel.text = post.timestampString
    }
    
    @objc
    func didTapComments() {
        guard let unwrappedPost = postCell else { return }
        commentTappedClosure?(unwrappedPost)
    }
    
    @objc
    func didTapLike() {
        guard let unwrappedPost = postCell else { return }
        didLikeTappedClosure?(unwrappedPost)
    }
    
    @objc
    func didTapShowUserProfile() {
        guard let unwrappedUser = postCell else { return }
        usernameTappedClosure?(unwrappedUser)
    }
}
