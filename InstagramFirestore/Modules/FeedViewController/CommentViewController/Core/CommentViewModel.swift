//
//  CommentViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 24/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase

protocol CommentViewModelProtocol: class {
    var delegate: CommentViewModelDelegate! { get set }
    
    var title: String { get }
    var dataSource: Int { get }
    
    func onViewDidLoad()
    func fetchComments()
    func item(at indexPath: IndexPath) -> Comment
    func uploadPost(_ comment: String)
    func showProfileController(withUid uid: String)
}

protocol CommentViewModelDelegate: class {
    func collectionViewReloadData()
    func clearCommentTextView()
    func showLoading(_ state: Bool)
    func showProfileController(show controller: ProfileViewController)
}

final class CommentViewModel {
    weak var delegate: CommentViewModelDelegate!
    
    private var comments = [Comment]()
    private let post: Post
    private let layout: UICollectionViewLayout
    private let tabBar = MainTabController()
    
    init(layout: UICollectionViewLayout, post: Post ) {
        self.layout = layout
        self.post = post
    }
}

extension CommentViewModel: CommentViewModelProtocol {
    func showProfileController(withUid uid: String) {
        UserService.fetchUserData(withUid: uid) { result in
            switch result {
            case .success(let profileUser):
                let profileViewModel = ProfileViewModel(layout: UICollectionViewFlowLayout(), user: profileUser)
                let controller = ProfileViewController(with: profileViewModel)
                self.delegate?.showProfileController(show: controller)
            case .failure(_):
                print("DEBUG: failed")
            }
        }
    }
    
    var title: String { Localized.CommentView.Title.navigation }
    var dataSource: Int { comments.count}
    
    func onViewDidLoad() {
        fetchComments()
    }
    
    func fetchComments() {
        CommentService.fetchComments(forPost: post.postId) { result in
            switch result {
            case .success(let comments):
                self.comments = comments
                self.delegate?.collectionViewReloadData()
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    func uploadPost(_ comment: String) {
        guard let currentUser = tabBar.user else { return }
        delegate.showLoading(true)
        CommentService.uploadComment(comment: comment, postID: post.postId, user: currentUser) { error in
            self.delegate.showLoading(false)
            self.delegate.clearCommentTextView()
            NotificationService.uploadNotification(toUid: self.post.ownerUid, fromUser: currentUser, type: .comment, post: self.post)
        }
    }
    
    func item(at indexPath: IndexPath) -> Comment {
        return comments[indexPath.row]
    }
}
