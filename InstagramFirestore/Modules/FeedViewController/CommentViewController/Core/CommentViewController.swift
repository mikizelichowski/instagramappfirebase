//
//  CommentViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 24/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class CommentViewController: UIViewController {
    private enum Constants {
        static let cellDynamicHeight: CGFloat = 45.0
        static let inputViewHeight: CGFloat = 50.0
    }
    
    private var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    private lazy var commentInputView: CommentInputAccessoryView = {
        let frame = CGRect(x: .zero, y: .zero, width: view.frame.width, height: Constants.inputViewHeight)
        let cv = CommentInputAccessoryView(frame: frame)
        cv.delegate = self
        return cv
    }()
    
    override var inputAccessoryView: UIView? {
        get { return commentInputView}
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    private var viewModel: CommentViewModelProtocol
    
    init(with viewModel: CommentViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionView()
        viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .black
        setupNavigation(false, title: viewModel.title)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigation(false, title: .empty)
        tabBarController?.tabBar.isHidden = false
    }
    
    private func configureCollectionView() {
        view.addSubview(collectionView)
        collectionView.fillSuperViewArea()
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(CommentCell.self)
        keyboardHide()
    }
    
    private func keyboardHide() {
        collectionView.alwaysBounceVertical = true
        collectionView.keyboardDismissMode = .interactive
    }
}

extension CommentViewController: CommentViewModelDelegate {
    func collectionViewReloadData() {
        collectionView.reloadData()
    }
}

extension CommentViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let uid = viewModel.item(at: indexPath).uid
        viewModel.showProfileController(withUid: uid)
    }
}

extension CommentViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataSource
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CommentCell = collectionView.dequeueReusableCell(indexPath: indexPath)
        cell.update(comment: viewModel.item(at: indexPath))
        return cell
    }
}

extension CommentViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// dynamic size
        let commentCell = CommentCell()
        commentCell.update(comment: viewModel.item(at: indexPath))
        let height = commentCell.dynamicSize(forWidth: view.frame.width).height + Constants.cellDynamicHeight
        return CGSize(width: view.frame.width, height: height)
    }
}

extension CommentViewController: CommentInputAccessoryViewDelegate {
    func inputView(_ inputView: CommentInputAccessoryView, wantsToUploadComment comment: String) {
        viewModel.uploadPost(comment)
    }
    
    func clearCommentTextView() {
        commentInputView.clearCommentTextView()
    }
    
    func showLoading(_ state: Bool) {
        showLoader(state)
    }
    
    func showProfileController(show controller: ProfileViewController) {
        navigationController?.pushViewController(controller, animated: true)
    }
}
