//
//  CommentCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 24/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class CommentCell: UICollectionViewCell {
    private enum Constants {
        static let lineWidth: CGFloat = 0.5
    }
    
    private var comment: Comment?
    private let profileImageView = CustomUIImage(style: .smallProfile)
    private let commentLabel = CustomLabel(style: .commentLabel)
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        backgroundColor = .white
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        [profileImageView, commentLabel].forEach { addSubview($0)}
        profileImageView.centerY(inView: self, leftAnchor: leftAnchor, paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
        commentLabel.centerY(inView: profileImageView, leftAnchor: profileImageView.rightAnchor, paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
        commentLabel.anchor(right: rightAnchor, paddingRight: CGFloat(StringRepresentationOfDigit.eight))
        BorderLayer.instantiate(view: self, lineWidth: Constants.lineWidth, strokeColor: .lineConnecting, borders: .top)
    }
    
    /// dynamic size
    func dynamicSize(forWidth width: CGFloat) -> CGSize {
        let label = UILabel()
        label.numberOfLines = .zero
        label.text = comment?.commentText
        label.lineBreakMode = .byWordWrapping
        label.setWidth(width)
        return label.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    func update(comment: Comment) {
        self.comment = comment
        guard let profileImageUrl = URL(string: comment.profileImageUrl) else { return }
        profileImageView.sd_setImage(with: profileImageUrl)
        let attributedString = NSMutableAttributedString(string: "\(comment.username) ", attributes: [.font: UIFont.font(with: .bold, size: .small)])
        attributedString.append(NSAttributedString(string: comment.commentText, attributes: [.font: UIFont.font(with: .regular, size: .small)]))
        commentLabel.attributedText = attributedString
    }
    
}

struct CommentCellData {
    let comment: String
}
