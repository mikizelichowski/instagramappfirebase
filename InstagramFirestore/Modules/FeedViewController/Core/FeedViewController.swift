//
//  FeedViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase

protocol LogoutDelegate: class {
    func logout()
}

final class FeedViewController: UIViewController {
    private var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    var post: Post?
    private var viewModel: FeedViewModelProtocol
    
    init(with viewModel: FeedViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .black
        setupNavigation(false, title: viewModel.title)
        if post == nil {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: viewModel.logoutTitle,
                                                               style: .plain,
                                                               target: self,
                                                               action: #selector(handleLogout))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigation(false, title: .empty)
    }
    
    private func configureUI() {
        view.addSubview(collectionView)
        collectionView.fillSuperViewArea()
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(FeedCell.self)
        refresher()
    }
    
    private func refresher() {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView.refreshControl = refresher
    }
    
    @objc
    func handleRefresh() {
        viewModel.send(action: .refreshPost(true))
    }
    
    @objc
    func handleLogout() {
        logoutAlert(title: .empty, message: Localized.FeedView.Alert.logoutMessage) { _ in
            self.viewModel.delegate?.logout()
        }
        self.logoutAlert(title: .empty, message: nil, handler: nil)
    }
}

extension FeedViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return post == nil ? viewModel.dataSourceCount : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FeedCell = collectionView.dequeueReusableCell(indexPath: indexPath)
        cell.commentTappedClosure = { [weak self] commentCell in
            self?.viewModel.showCommentController(wantsToShowCommentsFor: commentCell)
        }
        
        cell.didLikeTappedClosure = { [weak self] like in
            self?.viewModel.didTappedLike(cell, didLike: like)
        }
        
        cell.usernameTappedClosure = { [weak self] post in
            self?.viewModel.showProfileController(wantsToShowProfileFor: post.ownerUid)
        }
        
        if let post = post {
            cell.populate(with: post)
        } else {
            cell.populate(with: viewModel.feedItem(at: indexPath))
        }
        return cell
    }
}

extension FeedViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.width
        var height = width + 8 + 40 + 8
        height += 50
        height += 60
        return CGSize(width: width, height: height)
    }
}

extension FeedViewController: FeedViewModelDelegate {
    func showProfileController(show controller: ProfileViewController) {
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showCommentController(show controller: CommentViewController) {
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func reloadCollectionViewData() {
        collectionView.reloadData()
    }
    
    func refreshController() {
        collectionView.refreshControl?.endRefreshing()
    }
    
    func logout() {
        AuthService.logout()
        DispatchQueue.main.async {
            let viewModel = LoginViewModel()
            viewModel.authentication = self.tabBarController as? MainTabController
            let controller = LoginViewController(with: viewModel)
            let nav = UINavigationController(rootViewController: controller)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }
    }
}
