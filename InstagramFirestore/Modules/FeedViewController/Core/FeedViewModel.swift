//
//  FeedViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 28/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase

enum Action {
    case fetchPost
    case refreshPost(_ state: Bool)
}

protocol FeedViewModelProtocol: class {
    var delegate: FeedViewModelDelegate! { get set }
    
    var title: String { get }
    var logoutTitle: String { get }
    var dataSourceCount: Int { get }
    
    
    func onViewDidLoad()
    func feedItem(at indexPath: IndexPath) -> Post
    func send(action: Action)
    
    func didTappedLike(_ cell: FeedCell, didLike post: Post)
    func showProfileController(wantsToShowProfileFor uid: String)
    func showCommentController(wantsToShowCommentsFor post: Post)
}

protocol FeedViewModelDelegate: class {
    func logout()
    func reloadCollectionViewData()
    func refreshController()
    func showProfileController(show controller: ProfileViewController)
    func showCommentController(show controller: CommentViewController)
}

final class FeedViewModel {
    weak var delegate: FeedViewModelDelegate!
    
    var post: Post? {
        didSet { delegate.reloadCollectionViewData() }
    }

    var posts = [Post]()
    let tabBar = MainTabController()
    
    private let layout: UICollectionViewFlowLayout
    init(layout: UICollectionViewFlowLayout) {
        self.layout = layout
    }
}

extension FeedViewModel: FeedViewModelProtocol {
    var title: String { Localized.FeedView.Title.title }
    var logoutTitle: String { Localized.FeedView.Button.logout }
    #warning("fix")
    var dataSourceCount: Int { return posts.count }
    
    func onViewDidLoad() {
        send(action: .fetchPost)
        if post != nil { checkIfUserLikedPosts() }
    }
    #warning("fix")
    func feedItem(at indexPath: IndexPath) -> Post {
        let itemCount = posts[indexPath.row]
        return itemCount
    }
    
    func showCommentController(wantsToShowCommentsFor post: Post) {
        
        let viewModel = CommentViewModel(layout: UICollectionViewLayout(), post: post)
        let controller = CommentViewController(with: viewModel)
        self.delegate?.showCommentController(show: controller)
    }
    
    func showProfileController(wantsToShowProfileFor uid: String) {
        
        UserService.fetchUserData(withUid: uid) { [weak self] result in
            switch result {
            case .success(let user):
                let viewModel = ProfileViewModel(layout: UICollectionViewFlowLayout(), user: user)
                let controller = ProfileViewController(with: viewModel)
                self?.delegate?.showProfileController(show: controller)
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    #warning("fix")
    func didTappedLike(_ cell: FeedCell, didLike post: Post) {
        
        guard let user = tabBar.user else { return }
        cell.postCell?.didLike.toggle()
        if post.didLike {
            PostService.unlikePost(post: post) { _ in
                cell.likeButton.updateLikesButton(false)
                cell.postCell?.likes = post.likes - 1
            }
        } else {
            PostService.likePost(post: post) {  _ in
                cell.likeButton.updateLikesButton(true)
                cell.postCell?.likes = post.likes + 1
                NotificationService.uploadNotification(toUid: post.ownerUid,
                                                       fromUser: user,
                                                       type: .like, post: post)
            }
        }
    }
    
    func checkIfUserLikedPosts() {
        if let post = post {
            PostService.checkIfUserLikedPost(post: post) { didLike in
                self.post?.didLike = didLike
            }
        } else {
            posts.forEach { [weak self] post in
                PostService.checkIfUserLikedPost(post: post) { didLike  in
                    if let index = self?.posts.firstIndex(where: { $0.postId == post.postId }) {
                        self?.posts[index].didLike = didLike
                    }
                }
            }
        }
    }
    
    func send(action: Action) {
        switch action {
        case .fetchPost:
            guard post == nil else { return }
            PostService.fetchFeedPosts { posts in
                self.posts = posts
                self.checkIfUserLikedPosts()
                self.delegate.refreshController()
                self.delegate?.reloadCollectionViewData()
            }
        case .refreshPost(_):
            posts.removeAll()
            send(action: .fetchPost)
        }
    }
}

/// fetchuje tylko post osob, ktore followuje
//    func fetchPost() {
//        guard post == nil else { return }
//
//        PostService.fetchFeedPosts { posts in
//            self.posts = posts
//            self.checkIfUserLikedPosts()
//            self.delegate.refreshController()
//            self.delegate?.reloadCollectionViewData()
//        }
//    }


/// widziałem swoje rowniez posty
//        PostService.fetchPost { [weak self] result in
//            switch result {
//            case .success(let posts):
//                self?.posts = posts
//                self?.delegate?.refreshController()
//                self?.checkIfUserLikedPosts()
//                self?.delegate?.reloadCollectionViewData()
//            case .failure(let err):
//                print(err.localizedDescription)
//            }
//        }
