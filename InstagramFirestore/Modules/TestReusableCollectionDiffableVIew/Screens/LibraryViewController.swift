//
//  LibraryViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 13/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Combine

class LibraryViewController: UIViewController {
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return view
    }()
    
    private var tokens = [AnyCancellable]()
    
    private lazy var viewModel = LibraryViewModel(collectionView: collectionView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        collectionView.fillSuperView()
        configureCollectionView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
    }
    
    private func configureCollectionView() {
        collectionView.dataSource = viewModel.makeDataSource()
        collectionView.delegate = viewModel
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                  layout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width * 0.9, height: 100)
              }
    }
    
    @objc func addTapped() {
        let controller = NewNoteViewController()
        controller.notePublisher.sink { [weak self] note in
            guard let self = self else { return }
            self.viewModel.add([note])
        }
        .store(in: &tokens)
    }
}
