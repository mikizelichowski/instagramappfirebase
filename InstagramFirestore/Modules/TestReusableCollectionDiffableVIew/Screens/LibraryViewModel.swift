//
//  LibraryViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 13/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

class LibraryViewModel: CollectionViewModel<NoteCell> {
    init(collectionView: UICollectionView) {
        super.init(collectionView: collectionView, cellReuseIdentifier: "NoteCell")
    }
}

extension LibraryViewModel: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        remove([items.value[indexPath.item]])
    }
}
