//
//  NewNoteViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 13/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Combine

class NewNoteViewController: UIViewController {
    var textView = UITextView()
    
    public var notePublisher = PassthroughSubject<NoteDataSourceItem, Never>()
    
    private var note: NoteDataSourceItem {
        NoteDataSourceItem(text: textView.text)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textView.becomeFirstResponder()
    }
     
    @objc
    func saveTapped() {
        notePublisher.send(note)
        dismiss(animated: true, completion: nil)
    }
}
