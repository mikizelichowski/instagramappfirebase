//
//  Providable.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 13/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

protocol Providable {
    associatedtype ProvidedItem: Hashable
    func provide(_ item: ProvidedItem)
}
