//
//  Note.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 13/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

struct NoteDataSourceItem: Hashable {
    let text: String
    private let creationDate: Date
    
    init(text: String) {
        self.text = text
        self.creationDate = Date()
    }
    
    public var formattedDate: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        return formatter.string(for: text) ?? ""
    }
}
