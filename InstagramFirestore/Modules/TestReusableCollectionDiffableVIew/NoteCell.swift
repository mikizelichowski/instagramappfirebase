//
//  NoteCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 13/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

class NoteCell: UICollectionViewCell, Providable {
    typealias ProvidedItem = NoteDataSourceItem
    
    var textLabel = UILabel()
    var dateLabel = UILabel()
    
    public func provide(_ note: ProvidedItem) {
        self.textLabel.text = note.text
        self.dateLabel.text = note.text
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .quaternarySystemFill
        layer.cornerRadius = 16.0
    }
    
    // MARK: - Layout
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let layoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        layoutIfNeeded()
        layoutAttributes.frame.size = systemLayoutSizeFitting(layoutAttributes.frame.size, withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
        return layoutAttributes
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return CGSize(width: targetSize.width, height: contentView.systemLayoutSizeFitting(CGSize(width: contentView.bounds.width, height: 1)).height)
    }
}
