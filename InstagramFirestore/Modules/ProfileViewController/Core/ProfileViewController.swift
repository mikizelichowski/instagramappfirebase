//
//  ProfileViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class ProfileViewController: UIViewController {
    private enum Constants {
        static let headerHight: CGFloat = 240
    }
    
    private var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
    
    private var viewModel: ProfileViewModelProtocol
    private var profileViewModel: ProfileViewModel?
    
    init(with viewModel: ProfileViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionView()
        configureUI()
        viewModel.onViewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation(false, title: viewModel.getUser.username)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigation(false)
    }
    
    private func configureCollectionView() {
        collectionView.backgroundColor = .black
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(ProfileCell.self)
        collectionView.register(ProfileHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Localized.ProfileView.Identifier.title)
    }
    
    private func configureUI() {
        view.addSubview(collectionView)
        collectionView.fillSuperView()
    }
}

extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(StringRepresentationOfDigit.one)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(StringRepresentationOfDigit.one)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.width - CGFloat(StringRepresentationOfDigit.two)) / CGFloat(StringRepresentationOfDigit.three)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: Constants.headerHight)
    }
}

extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numbersOfRows()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ProfileCell = collectionView.dequeueReusableCell(indexPath: indexPath)
        cell.update(posts: viewModel.postItem(at: indexPath))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectItem(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Localized.ProfileView.Identifier.title, for: indexPath) as! ProfileHeader
        header.delegateButton = self
        header.viewModels = ProfileHeaderViewModel(user: viewModel.getUser)
        return header
    }
}

extension ProfileViewController: ProfileViewModelDelegate {
    func numbersOfRows() -> Int {
        return viewModel.dataSourceCount
    }
    
    func reloadCollectionView() {
        collectionView.reloadData()
    }
}

extension ProfileViewController: ProfileHeaderDelegate {
    func showFeedViewController(with controller: FeedViewController){
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func header(_ profileHeader: ProfileHeader, didTapActionButtonFor user: User) {
        //guard let tab = tabBarController as? MainTabController else { return }
        //guard let currentUser = tab.user else { return }
        
        if user.isCurrentUser {
            print("DEBUG: Show edit profile here")
        } else if user.isFollowed {
            UserService.unfollow(uid: user.uid) { error in
                self.viewModel.isFollowed(false)
                self.collectionView.reloadData()
                PostService.updateUserFeedAfterFollowing(user: self.viewModel.getUser, didFollow: false)
            }
        } else {
            UserService.follow(uid: user.uid) { error in
                self.viewModel.isFollowed(true)
                self.collectionView.reloadData()
                NotificationService.uploadNotification(toUid: user.uid,
                                                       fromUser: self.viewModel.getUser,
                                                       type: .follow)
                PostService.updateUserFeedAfterFollowing(user: self.viewModel.getUser, didFollow: true)
            }
        }
    }
}
