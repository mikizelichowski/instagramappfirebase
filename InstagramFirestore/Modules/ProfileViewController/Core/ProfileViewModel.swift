//
//  ProfileViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 31/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase

protocol ProfileHeaderDelegate: class {
    func header(_ profileHeader: ProfileHeader, didTapActionButtonFor user: User)
}

protocol ProfileViewModelProtocol: class {
    var delegate: ProfileViewModelDelegate! { get set }
    
    var title: String { get }
    var dataSourceCount: Int { get }
    var getUser: User { get }
    var layout: UICollectionViewFlowLayout { get }
    
    func onViewDidLoad()
    func isFollowed(_ state: Bool)
    
    func fetchData()
    func item(at indexPath: IndexPath) -> User
    func selectItem(at indexPath: IndexPath)
    
    func postItem(at indexPath: IndexPath) -> Post
}

protocol ProfileViewModelDelegate: class {
    func reloadCollectionView()
    func numbersOfRows() -> Int
    func showFeedViewController(with controller: FeedViewController)
}

final class ProfileViewModel {
    weak var delegate: ProfileViewModelDelegate!
    
    var user: User { didSet { delegate.reloadCollectionView() }}
    var posts = [Post]() { didSet { delegate.reloadCollectionView() }}
    private var dataSource: [User] = []
    internal let layout: UICollectionViewFlowLayout
    
    init(layout: UICollectionViewFlowLayout, user: User) {
        self.layout = layout
        self.user = user
    }
    
    func checkIfUserIsFollowed() {
        UserService.checkIfUserIsFollowed(uid: user.uid) { isFollowed in
            self.user.isFollowed = isFollowed
            self.delegate.reloadCollectionView()
        }
    }
    
    func fetchUserStats() {
        UserService.fetchUserStats(uid: user.uid) { stats in
            self.user.stats = stats
            //self.delegate.reloadCollectionView()
        }
    }
    
    func fetchPosts() {
        PostService.fetchPosts(forUser: user.uid) { result in
            switch result {
            case .success(let posts):
                self.posts = posts
//                self.delegate.reloadCollectionView()
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
}

extension ProfileViewModel: ProfileViewModelProtocol {
    var title: String { Localized.ProfileView.Titel.title }
    var dataSourceCount: Int { return posts.count}
    var getUser: User { return user }

    func onViewDidLoad() {
        
        checkIfUserIsFollowed()
        fetchUserStats()
        fetchPosts()
    }
    
    func fetchData() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        UserService.fetchUserData(withUid: uid) { result in
            switch result {
            case .success(let user):
                self.user = user
                self.delegate.reloadCollectionView()
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    func isFollowed(_ state: Bool) {
        user.isFollowed = state
    }
    
    func item(at indexPath: IndexPath) -> User {
        return dataSource[indexPath.row]
    }
    
    func postItem(at indexPath: IndexPath) -> Post {
        return posts[indexPath.row]
    }
    
    func selectItem(at indexPath: IndexPath) {
        let viewModel = FeedViewModel(layout: layout)
        let controller = FeedViewController(with: viewModel)
        controller.post = postItem(at: indexPath)
        delegate.showFeedViewController(with: controller)
    }
}
