//
//  ProfileHeaderViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 03/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

struct ProfileHeaderViewModel {
    private enum Constants {
        static let followButtonTitle = "Edit profile"
        static let followingTitle = "Following"
        static let followTitle = "Follow"
        static let followingLabel = "following"
        static let followersLabel = "followers"
        static let postsLabel = "posts"
    }
    
    let user: User
    
    var fullname: String { return user.fullname }
    var profileImageUrl: URL? { return URL(string: user.profileImageUrl) }
    
    var followButtonText: String {
        if user.isCurrentUser {
            return Constants.followButtonTitle
        }
        return user.isFollowed ? Constants.followingTitle : Constants.followTitle
    }
    
    var followButtonBackgroundColor: UIColor { return user.isCurrentUser ? .white : .systemBlue }
    var followButtonTextColor: UIColor { return user.isCurrentUser ? .black : .white }
    var numberOfFollowers: NSAttributedString { return attributedStatText(value: user.stats.followers, label: Constants.followersLabel) }
    var numberOfFollowing: NSAttributedString { return attributedStatText(value: user.stats.following, label: Constants.followingLabel) }
    var numberofPosts: NSAttributedString { return attributedStatText(value: user.stats.posts, label: Constants.postsLabel) }
    
    init(user: User) {
        self.user = user
    }
    
    private func attributedStatText(value: Int, label: String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: "\(value)\n", attributes: [.font: UIFont.font(with: .bold, size: .small)])
        attributedText.append(NSAttributedString(string: label, attributes: [.font: UIFont.font(with: .regular, size: .small), .foregroundColor: UIColor.lightGray]))
        return attributedText
    }
    
}
