//
//  ProfileCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 31/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class ProfileCell: UICollectionViewCell {
//final class ProfileCell: DataSourceCell<CommonViewDatasourceItem> {
    private let postImageView = CustomUIImage(style: .post)

    override init(frame: CGRect) {
        super.init(frame: frame)
    
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(postImageView)
        postImageView.fillSuperView()
    }

    func update(posts: Post){
        guard let imageUrl = URL(string: posts.imageUrl) else { return }
        postImageView.sd_setImage(with: imageUrl)
    }
}

