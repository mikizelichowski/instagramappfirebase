//
//  ProfileHeader.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 31/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import SDWebImage

//final class ProfileHeader: UICollectionReusableView {
final class ProfileHeader: CollectionHeaderDataSource<CommonViewDatasourceItem> {
    private enum Constants {
        static let magicNumberTwelve: CGFloat = 12
        static let magicNumberFifteen: CGFloat = 15
        static let magicNumberSixteen: CGFloat = 16
        static let magicNumberTwentyFour: CGFloat = 24
        static let StackHeight: CGFloat = 50
        static let lineWidth: CGFloat = 0.5
        static let cornerRadius: CGFloat = 8.0
        static let loadingTitle = "Loading"
    }
    
    var viewModels: ProfileHeaderViewModel? {
        didSet { configure() }
    }
    
    weak var delegateButton: ProfileHeaderDelegate?
    
    private let profileImageView = CustomUIImage(style: .profile)
    private let nameLabel = CustomLabel(style: .defaultLabel)
    private lazy var editProfileFollowButton = CustomButton(title: Constants.loadingTitle, style: .editButton)
    private lazy var postLabel = CustomLabel(style: .profileLabel)
    private lazy var followingLabel = CustomLabel(style: .profileLabel)
    private lazy var followersLabel = CustomLabel(style: .profileLabel)
    private let girdButton = CustomButton(style: .iconButton, image: Asset.grid.image)
    private let listButton = CustomButton(style: .iconButtonSelect, image: Asset.list.image)
    private let bookmarkButton = CustomButton(style: .iconButtonSelect, image: Asset.ribbon.image)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
       setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func handleEditProfileFollowTapped() {
        
        guard let viewModel = viewModels else { return }
        delegateButton?.header(self, didTapActionButtonFor: viewModel.user)
    }
    
    private func setupView() {
        
        setupLabels()
        backgroundColor = .white
        [profileImageView, nameLabel, editProfileFollowButton].forEach{addSubview($0)}
        profileImageView.anchor(top: topAnchor,
                                left: leftAnchor,
                                paddingTop: Constants.magicNumberSixteen,
                                paddingLeft: Constants.magicNumberTwelve)
        nameLabel.anchor(top: profileImageView.bottomAnchor,
                         left: leftAnchor,
                         paddingTop: Constants.magicNumberTwelve,
                         paddingLeft: Constants.magicNumberTwelve)
        editProfileFollowButton.anchor(top: nameLabel.bottomAnchor,
                                       left: leftAnchor,
                                       right: rightAnchor,
                                       paddingTop: Constants.magicNumberSixteen,
                                       paddingLeft: Constants.magicNumberTwentyFour,
                                       paddingRight: Constants.magicNumberTwentyFour)
        let stack = UIStackView(arrangedSubviews: [postLabel, followersLabel, followingLabel])
        stack.distribution = .fillEqually
        addSubview(stack)
        stack.centerY(inView: profileImageView)
        stack.anchor(left: profileImageView.rightAnchor,
                     right: rightAnchor,
                     paddingLeft: Constants.magicNumberTwelve,
                     paddingRight: Constants.magicNumberTwelve,
                     height: Constants.StackHeight)
        let topView = UIView()
        addSubview(topView)
        topView.anchor(left: leftAnchor,
                       bottom: bottomAnchor,
                       right: rightAnchor,
                       paddingBottom: CGFloat(StringRepresentationOfDigit.five))
        let buttonStack = UIStackView(arrangedSubviews: [girdButton, listButton, bookmarkButton])
        buttonStack.distribution = .fillEqually
        buttonStack.alignment = .center
        topView.addSubview(buttonStack)
        BorderLayer.instantiate(view: topView, lineWidth: Constants.lineWidth, strokeColor: .lineConnecting, borders: .top)
        buttonStack.anchor(top: topView.topAnchor,
                           left: topView.leftAnchor,
                           bottom: topView.bottomAnchor,
                           right: topView.rightAnchor,
                           paddingTop: Constants.magicNumberFifteen,
                           paddingBottom: CGFloat(StringRepresentationOfDigit.five))
    }
    
    private func setupLabels() {
        
        editProfileFollowButton.addTarget(self, action: #selector(handleEditProfileFollowTapped), for: .touchUpInside)
    }
    
    func configure() {
        
        guard let viewModel = viewModels else { return }
        nameLabel.text = viewModel.fullname
        profileImageView.sd_setImage(with: viewModel.profileImageUrl)
        editProfileFollowButton.setTitle(viewModel.followButtonText, for: .normal)
        editProfileFollowButton.setTitleColor(viewModel.followButtonTextColor, for: .normal)
        editProfileFollowButton.backgroundColor = viewModel.followButtonBackgroundColor
        followingLabel.attributedText = viewModel.numberOfFollowing
        followersLabel.attributedText = viewModel.numberOfFollowers
        postLabel.attributedText = viewModel.numberofPosts
    }
}
