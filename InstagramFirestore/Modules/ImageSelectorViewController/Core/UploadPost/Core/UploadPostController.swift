//
//  UploadPostController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 19/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class UploadPostController: UIViewController {
    private enum Constants {
        static let imageDimension: CGFloat = 180.0
    }
    
    var selectedImage: UIImage? {
        didSet { photoImageView.image = selectedImage }
    }
    
    var viewModel: UploadPostViewModelProtocol
    
    init(with viewModel: UploadPostViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var captionTextView = InputTextView()
    private let photoImageView = CustomUIImage(style: .post, width: Constants.imageDimension, height: Constants.imageDimension)
    private let characterCountLabel = CustomLabel(style: .lightLabel, title: "\(StringRepresentationOfDigit.zero)/\(StringRepresentationOfDigit.hundred)")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = Localized.UploadPost.Title.navigation
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didTapCancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: Localized.UploadPost.Title.shareItem, style: .done, target: self, action: #selector(didTapDone))
    }
    
    private func configureUI() {
        
        view.backgroundColor = .white
        [photoImageView, captionTextView, characterCountLabel].forEach{ view.addSubview($0)}
        photoImageView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                              paddingTop: CGFloat(StringRepresentationOfDigit.eight))
        photoImageView.centerX(inView: view)
        captionTextView.anchor(top: photoImageView.bottomAnchor,
                               left: view.leftAnchor, right: view.rightAnchor,
                               paddingTop: CGFloat(StringRepresentationOfDigit.hundred),
                               paddingLeft: CGFloat(StringRepresentationOfDigit.twelve),
                               paddingRight: CGFloat(StringRepresentationOfDigit.twelve))
        captionTextView.setHeight(CGFloat(StringRepresentationOfDigit.hour))
        characterCountLabel.anchor(bottom: captionTextView.bottomAnchor,
                                   right: view.rightAnchor,
                                   paddingBottom: CGFloat(-StringRepresentationOfDigit.eight),
                                   paddingRight: CGFloat(StringRepresentationOfDigit.twelve))
        setupLabel()
    }
    
    private func setupLabel() {
        photoImageView.update(.post)
        captionTextView.setupText(.createPost)
        captionTextView.delegate = self
        captionTextView.placeholderShouldCenter = false
    }
    
    @objc private func didTapCancel() {
        viewModel.delegate.didTapCanceled()
    }
    
    @objc private func didTapDone() {
        guard let image = selectedImage else { return }
        guard let captionText = captionTextView.text else { return }
        viewModel.didTapDone(selectedImage: image, caption: captionText)
    }
}

extension UploadPostController: UploadPostViewModelDelegate {
    func didTapCanceled() { // do poprawki przechodzenie
        print("DEBUG: did tap cancel")
        let mainTab = MainTabController()
        mainTab.selectedIndex = .zero
        navigationController?.pushViewController(mainTab, animated: true)
    }
    
    private func showsLoader(_ state: Bool) {
        showLoader(state)
    }
}

extension UploadPostController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        viewModel.checkMaxLength(textView)
        let count = textView.text.count
        characterCountLabel.text = "\(count)/\(StringRepresentationOfDigit.hundred)"
    }
}
