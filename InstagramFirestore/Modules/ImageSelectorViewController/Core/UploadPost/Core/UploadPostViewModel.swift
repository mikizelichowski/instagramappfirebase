//
//  UploadPostViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 21/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol UploadPostViewModelProtocol: class {
    var delegate: UploadPostViewModelDelegate! { get set }
    
    func didTapDone(selectedImage: UIImage, caption: String)
    func checkMaxLength(_ textView: UITextView)
}

protocol UploadPostViewModelDelegate: class {
    func didTapCanceled()
    func showLoader(_ state: Bool)
}

final class UploadPostViewModel {
    weak var delegate: UploadPostViewModelDelegate!
    weak var delegatePost: ControllerDidFinishUploadingPostDelegate!
    var currentUser: User?
}

extension UploadPostViewModel: UploadPostViewModelProtocol {
    func didTapDone(selectedImage: UIImage, caption: String) {
        guard let user = currentUser else { return }
        delegate.showLoader(true)
        PostService.uploadPost(caption: caption, image: selectedImage, user: user) { error in
            self.delegate.showLoader(false)
            if let error = error {
                print("DEBUG: Failed to upload post with error \(error.localizedDescription)")
                return
            }
            self.delegatePost?.controllerDidFinishUploadingDelegate()
        }
    }
    
    func checkMaxLength(_ textView: UITextView) {
        if (textView.text.count) > StringRepresentationOfDigit.hundred {
            textView.deleteBackward()
        }
     }
}
