//
//  ImageSelectorViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class ImageSelectorViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
    }
}
