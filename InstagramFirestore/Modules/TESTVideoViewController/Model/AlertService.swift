//
//  AlertService.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 10/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

struct AlertService {
    
    func createUserAlert(completion: @escaping(String) -> Void ) -> UIAlertController {
        let alert = UIAlertController(title: "Create User", message: nil, preferredStyle: .alert)
        alert.addTextField { $0.placeholder = "Enter user's name"}
        let action = UIAlertAction(title: "Save", style: .default) { _ in
            let userName = alert.textFields?.first?.text ?? ""
        completion(userName)
        }
        alert.addAction(action)
        return alert
    }
}
