//
//  DataSourceController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 02/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

#warning("Test")

final class DataSourceController: UIViewController {
    enum Section {
        case first
        case second
    }
    
    struct Fruit: Hashable {
        let title: String
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(title)
        }
        
        static func == (lhs: Fruit, rhs: Fruit) -> Bool {
            return lhs.title == rhs.title
        }
    }
    
    let tableView = UITableView()

    var dataSource: UITableViewDiffableDataSource<Section, Fruit>!
    //var fruits = [Fruit]()
    var fruits: [Fruit] = []
    var sections: [Section] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupTableView()
        dataSourceTableView()
    }
    
    private func setupTableView() {
        tableView.registerTableViewCell(UITableViewCell.self)
        
        view.addSubview(tableView)
        tableView.fillSuperView()
        tableView.delegate = self
       // tableView.frame = view.bounds
    }
    
    func dataSourceTableView() {
        dataSource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, model) -> UITableViewCell? in
            let cell: UITableViewCell = tableView.dequeueReusableTablewViewCell(indexPath: indexPath)
            cell.textLabel?.text = model.title
            return cell
        })
        
        title = "My fruits"
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus"), style: .done, target: self, action: #selector(didTapAdd))
    }
    
    @objc func didTapAdd() {
        let actionSheet = UIAlertController(title: "Select fruit", message: nil, preferredStyle: .actionSheet)
        for x in 0...100 {
            actionSheet.addAction(UIAlertAction(title: " Fruits \( x+1)", style: .default, handler: { [weak self] _ in
                let fruit = Fruit(title: "Fruits \(x+1)")
                self?.fruits.append(fruit)
                self?.updateDataSource()
            }))
        }
        
        present(actionSheet, animated: true)
    }
    
    func updateDataSource() {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Fruit>()
        snapshot.appendSections([.first, .second])
        snapshot.appendItems(fruits)
        snapshot.appendItems(fruits, toSection: .second)
        dataSource.apply(snapshot, animatingDifferences: true, completion: nil)
    }
}

extension DataSourceController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let fruit = dataSource.itemIdentifier(for: indexPath) else { return }
        print("DEBUG: fruit \(fruit.title)")
    }
}
