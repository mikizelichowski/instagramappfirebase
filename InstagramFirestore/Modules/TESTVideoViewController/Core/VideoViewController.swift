//
//  VideoViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 10/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class VideoViewController: UICollectionViewController {
    enum Section {
        case main
    }
    
    private var users = [User]()
    
    // TODO: DataSource & DataSourceSnapshot typealias
    typealias DataSource = UICollectionViewDiffableDataSource<Section, User>
    typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Section, User>
    
    // TODO: dataSource & Snapshot
    private var dataSource: DataSource!
    private var snapshot = DataSourceSnapshot()
    
//    private var collectionView = UICollectionView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        configureCollectionViewDataSource()
        configureCollectionViewLayout()
        fetchData()
    }
    
    private func createLayout() -> UICollectionViewLayout {
        // TODO: Item
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8)
        
        // TODO: Group
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(76))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        
        // TODO: Section
        let section = NSCollectionLayoutSection(group: group)
        
        // TODO: Layout
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    private func configureCollectionViewDataSource() {
        // TODO: dataSource
        dataSource = DataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, user) -> VideoViewCell? in
            let cell: VideoViewCell = collectionView.dequeueReusableCell(indexPath: indexPath)
            cell.configure(with: user)
            return cell
        })
    }
    
    //    private func createDummyData() {
    //        var dummyContacts: [User] = []
    //        for i in 0..<10 {
    //            dummyContacts.append(User(dictionary: <#T##[String : Any]#>))
    //        }
    //        applySnapshot(user: dummyContacts)
    //    }
    
    //    @objc
    //    private func didTapAddButton() {
    //        let alert = AlertService.createUserAlert { [weak self] name in
    //            self?.addNewUser(with: name)
    //        }
    //        present(alert, animated: true)
    //    }
    
    private func fetchData() {
        UserService.fetchUsersData { (result) in
            switch result {
            case .success(let users):
                self.applySnapshot(user: users)
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    func addNewUser(with name: [String: Any]) {
        let user = User(dictionary: name)
        users.append(user)
        applySnapshot(user: users)
    }
    
    private func applySnapshot(user: [User]) {
        snapshot = DataSourceSnapshot()
        snapshot.appendSections([Section.main])
        snapshot.appendItems(user)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    private func configureCollectionViewLayout() {
       // collectionView = UICollectionView(frame: view.frame, collectionViewLayout: createLayout())
        collectionView.delegate = self
        collectionView.dataSource = dataSource
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        collectionView.registerCell(VideoViewCell.self)
        view.addSubview(collectionView)
        collectionView.fillSuperView()
    }
}

extension VideoViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let user = dataSource.itemIdentifier(for: indexPath) else { return }
        print(user)
    }
}

struct Users: Hashable {
    
}
