//
//  VideoViewCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 13/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class VideoViewCell: UICollectionViewCell {
    
    private let label: UILabel = {
       let label = UILabel()
        label.textColor = .black
        label.font = .font(with: .bold, size: .medium)
        return label
    }()
    
    private let labelSecond: UILabel = {
       let label = UILabel()
        label.textColor = .black
        label.font = .font(with: .bold, size: .medium)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with model: User) {
        label.text = model.username
        labelSecond.text = model.fullname
    }
    
    func setupUI() {
        [label, labelSecond].forEach { addSubview($0)}
        label.centerY(inView: self, leftAnchor: leftAnchor, paddingLeft: 12)
        labelSecond.centerY(inView: self, leftAnchor: label.rightAnchor, paddingLeft: 2)
    }
}
