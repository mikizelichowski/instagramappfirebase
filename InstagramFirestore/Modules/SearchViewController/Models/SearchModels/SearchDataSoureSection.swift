//
//  SearchDataSoureSection.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 03/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

struct SearchDataSourceSection: DataSourceSection {
    let identity: String
    let viewIdentifier: String?
    let items: [SearchDataSourceItem]
    let title: String?
}

enum SearchSectionType: CaseIterable {
    case main
}
