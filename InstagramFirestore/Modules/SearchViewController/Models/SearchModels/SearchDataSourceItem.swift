//
//  SearchDataSourceItem.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 03/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

struct SearchDataSourceItem: DataSourceIdentifiable & Equatable & Hashable {
    let identity: Int
    let viewIdentifier: String?
    let username: String
    let fullname: String
    let image: UIImage?
    var user: User?
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(viewIdentifier)
    }
    
    static func == (lhs: SearchDataSourceItem, rhs: SearchDataSourceItem) -> Bool {
        return lhs.identity == rhs.identity
    }
}
