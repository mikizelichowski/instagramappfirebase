//
//  UserCellViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 09/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

#warning("I use this for second option")

struct UserCellViewModel {
    //Cannot assign value of type 'User' to type 'UserCellViewModel'
    private let user: User
    
    var profileImageUrl: URL? {
        return URL(string: user.profileImageUrl)
    }
    
    var username: String {
        return user.username
    }
 
    var fullname: String {
        return user.fullname
    }
    
    init(user: User) {
        self.user = user
    }
}
