//
//  SearchController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 03/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
#warning("Test !!!!!!!!")


enum SingleSection {
    case main
}

class CollectionViewManager<Section: Hashable, Item: Hashable>: NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var cellForRow: UICollectionViewDiffableDataSource<Section, Item>.CellProvider?
    private var dataSource: UICollectionViewDiffableDataSource<Section, Item>?
    
    
//    var selectedItemPublisher = PassthroughSubject<Item, Never>()
//    var willDisplayCellPublisher = PassthroughSubject<(cell: UICollectionViewCell, indexPath: IndexPath), Never>()
//    var cellDisappearedPublisher = PassthroughSubject<(cell: UICollectionViewCell, indexPath: IndexPath), Never>()
    
    private var collectioView: UICollectionView?
    
    
    
    func manage(_ collectionView: UICollectionView) {
        collectionView.delegate = self
        
        self.collectioView = collectionView
        
        dataSource = UICollectionViewDiffableDataSource(collectionView: collectionView) { [weak self ] in
            self?.cellForRow?($0, $1, $2)
        }
    }
    
    private func apply(_ change: (inout NSDiffableDataSourceSnapshot<Section, Item>) -> Void) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Item>()
        change(&snapshot)
        dataSource?.apply(snapshot)
    }
    
    func set(_ items: [Item], in section: Section) {
        apply {
            $0.appendSections([section])
            $0.appendItems(items)
        }
    }
    
    func append(_ item: Item, in section: Section) {
        guard var currentSnapshot = dataSource?.snapshot() else { return }
        currentSnapshot.appendItems([item], toSection: section)
        dataSource?.apply(currentSnapshot)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        willDisplayCellPublisher.send((cell, indexPath))
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        cellDisappearedPublisher.send((cell, indexPath))
    }
}

extension CollectionViewManager where Section == SingleSection {
    func set(_ items: [Item]) {
        self.set(items, in: .main)
    }
    
    func append(_ item: Item) {
        self.append(item, in: .main)
    }
}
 

#warning(" !!!!! ")

final class SearchController: UIViewController {
    enum Section: Int, CaseIterable {
        case main
    }
    
    private let collectionView = UICollectionView()
    private lazy var dataSource = makeDataSource()
    private var searchController = UISearchController(searchResultsController: nil)
    private var sections: [Section] = Section.allCases
    // or
//    private var sections: [Section] = []
    private var users: [User] = []
    
    typealias DataSource = UICollectionViewDiffableDataSource<Section, User>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, User>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     setup()
    }
    
    private func setup() {
        collectionView.registerCell(UserViewCell.self)
        collectionView.dataSource = dataSource
        apllySnapshot(animatingDifferences: false)
    }
    
    func makeDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, user) -> UICollectionViewCell? in
            let cell: UserViewCell = collectionView.dequeueReusableCell(indexPath: indexPath)
            cell.setup(with: user)
            return cell
        })
        return dataSource
    }
    
    func apllySnapshot(animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        snapshot.appendSections(sections)
        snapshot.appendItems(users)
//        snapshot.appendSections([.main])
//        sections.forEach{ section in
//            snapshot.appendItems(section.video, toSection: section)
//        }
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
}

extension SearchController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let user = dataSource.itemIdentifier(for: indexPath) else { return }
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}
