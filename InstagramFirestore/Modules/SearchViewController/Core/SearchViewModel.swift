//
//  SearchViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 06/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol SearchViewModelProtocol: class {
    var delegate: SearchViewModelDelegate! { get set }
    
    var dataPostCount: Int { get }
    var dataSourceCount: Int { get }
    var filteredCount: Int { get }
    
    func onViewDidLoad()
    func fetchData()
    func filterData(searchText: String)
    func filterItem(at indexPath: IndexPath) -> User
    func item(at indexPath: IndexPath) -> User
    func itemPosts(at indexPath: IndexPath) -> Post
    func selectItem(at indexPath: IndexPath) 
}

protocol SearchViewModelDelegate: class {
    func reloadDataTableView()
    func reloadDataCollectionView()
    func showProfileViewController(with user: User)
    func showFeedViewController(with controller: FeedViewController)
}

final class SearchViewModel {
    weak var delegate: SearchViewModelDelegate!
    
    private var posts = [Post]()
    private var users = [User]()
    private var dataSource: [User] = []
    private var filterDataSource: [User] = []
    
    private func parseData(_ data: User) {
        /*   private func parseData(_ data: LanguagesResponseModel) {
         dataSource = data.languages.map { LanguageRenderable(language: $0) }
         delegate.reloadData()
         }
         */
    }
}

extension SearchViewModel: SearchViewModelProtocol {
    func filterData(searchText: String) {
        filterDataSource = users.filter({$0.username.contains(searchText) || $0.fullname.lowercased().contains(searchText)})
        delegate.reloadDataTableView()
    }
    
    func onViewDidLoad() {
        fetchData()
        fetchPosts()
    }
    
    func fetchData() {
        dataSource.removeAll()
        UserService.fetchUsersData { (result) in
            switch result {
            case .success(let users):
                self.users = users
                DispatchQueue.main.async {
                self.delegate.reloadDataTableView()
                }
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    func fetchPosts() {
        PostService.fetchPost { result in
            switch result {
            case .success(let posts):
                self.posts = posts
                self.delegate.reloadDataCollectionView()
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    var dataPostCount: Int { return posts.count }
    
    func itemPosts(at indexPath: IndexPath) -> Post {
        return posts[indexPath.row]
    }
    
    func item(at indexPath: IndexPath) -> User {
        return users[indexPath.row]
    }
    
    func filterItem(at indexPath: IndexPath) -> User {
        return filterDataSource[indexPath.row]
    }
    
    var dataSourceCount: Int {
        return users.count
    }
    
    var filteredCount: Int {
        return filterDataSource.count
    }
    
    func selectItem(at indexPath: IndexPath) {
        let viewModel = FeedViewModel(layout: UICollectionViewFlowLayout())
        let controller = FeedViewController(with: viewModel)
        controller.post =  itemPosts(at: indexPath)//postItem(at: indexPath)
        delegate.showFeedViewController(with: controller)
    }


}

protocol UserWorkerProtocol {
    func getUserProvider(providers: [String: Any]) -> User
}

final class UserWorker: UserWorkerProtocol {
    func getUserProvider(providers: [String : Any]) -> User {
        let provider = User(dictionary: providers)
        return provider
    }
}
