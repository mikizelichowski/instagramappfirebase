//
//  SearchViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class SearchViewController: UIViewController {
    private enum Constants {
        static let cellHeight: CGFloat = 80.0
    }
    
    private let tableView = UITableView()
    private let layout = UICollectionViewFlowLayout()
    private lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var inSearchMode: Bool {
        return searchController.isActive && !searchController.searchBar.text!.isEmpty
    }
    
    private var viewModel: SearchViewModelProtocol
    
    init(with viewModel: SearchViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSearchController()
        configureTableView()
        configureCollectionView()
        viewModel.onViewDidLoad()
    }
    
    private func configureTableView() {
        
        view.backgroundColor = .black
        view.addSubview(tableView)
        tableView.fillSuperView()
        tableView.rowHeight = Constants.cellHeight
        tableView.registerTableViewCell(UserCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
    }
    
    private func configureCollectionView() {
        
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.fillSuperViewArea()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(ProfileCell.self)
    }
    
    private func configureSearchController() {
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.placeholder = Localized.SearchView.Title.search
        navigationItem.searchController = searchController
        definesPresentationContext = false
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return inSearchMode ? viewModel.filteredCount : viewModel.dataSourceCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UserCell = tableView.dequeueReusableTablewViewCell(indexPath: indexPath)
        cell.backgroundColor = .white
        let user = inSearchMode ? viewModel.filterItem(at: indexPath) : viewModel.item(at: indexPath)
        cell.setup(with: user)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectData = inSearchMode ? viewModel.filterItem(at: indexPath) : viewModel.item(at: indexPath)
        viewModel.delegate.showProfileViewController(with: selectData)
    }
}

// MARK: - SearchViewModelDelegate

extension SearchViewController: SearchViewModelDelegate {
    func reloadDataTableView() {
        tableView.reloadData()
    }
    
    func reloadDataCollectionView() {
        collectionView.reloadData()
    }
    
    func showFeedViewController(with controller: FeedViewController) {
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showProfileViewController(with user: User) {
        
        let viewModel = ProfileViewModel(layout: layout, user: user)
        let controller = ProfileViewController(with: viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - UISearchBarDelegate & UISearchResultsUpdating

extension SearchViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
        collectionView.isHidden = true
        tableView.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.showsCancelButton = false
        searchBar.text = nil
        collectionView.isHidden = false
        tableView.isHidden = true
    }
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text?.lowercased() else { return }
        viewModel.filterData(searchText: searchText)
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataPostCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProfileCell = collectionView.dequeueReusableCell(indexPath: indexPath)
        cell.update(posts: viewModel.itemPosts(at: indexPath))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectItem(at: indexPath)
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(StringRepresentationOfDigit.one)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(StringRepresentationOfDigit.one)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.width - CGFloat(StringRepresentationOfDigit.two)) / CGFloat(StringRepresentationOfDigit.three)
        return CGSize(width: width, height: width)
    }
}
