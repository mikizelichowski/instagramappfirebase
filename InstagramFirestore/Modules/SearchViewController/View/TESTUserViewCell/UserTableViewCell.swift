//
//  UserTableViewCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 03/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class UserViewCell: UICollectionViewCell {
    private enum Constants {
        static let lineWidth: CGFloat = 0.5
    }
    
    private let titleLabel = CustomLabel(style: .postLabel)
    private let subtitleLabel = CustomLabel(style: .profileLabel)
    private var profileImage = CustomUIImage(style: .smallProfile)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
        
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        BorderLayer.instantiate(view: self,
                                lineWidth: Constants.lineWidth,
                                strokeColor: .lineConnecting,
                                borders: .top)
        addSubview(profileImage)
        profileImage.centerY(inView: self,
                             leftAnchor: leftAnchor,
                             paddingLeft: CGFloat(StringRepresentationOfDigit.twelve))
        let stack = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stack.axis = .vertical
        stack.spacing = CGFloat(StringRepresentationOfDigit.four)
        stack.alignment = .leading
        addSubview(stack)
        stack.centerY(inView: profileImage,
                      leftAnchor: profileImage.rightAnchor,
                      paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
        setupLabel()
    }
    
    private func setupLabel() {
        [subtitleLabel].forEach {
            $0.font = .font(with: .regular, size: .small)
            $0.textColor = .lightGray
        }
    }
    
    func setup(with user: User) {
        guard let imageUrl = URL(string: user.profileImageUrl) else { return }
        profileImage.sd_setImage(with: imageUrl)
        titleLabel.text = user.username
        subtitleLabel.text = user.fullname
    }
}
