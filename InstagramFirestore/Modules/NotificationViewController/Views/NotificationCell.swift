//
//  NotificationCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

//final class NotificationCell: CollectionCellDataSource<NotificationDataSourceItem>

final class NotificationCell: UITableViewCell {
    private enum Constants {
        static let lineWidth: CGFloat = 0.5
    }
    
    private lazy var profileImageView = CustomUIImage(style: .smallProfileComments)
    private let infoLabel = CustomLabel(style: .commentLabel)
    private lazy var postImageView = CustomUIImage(style: .notification)
    private lazy var followButton = CustomButton(title: "Loading", style: .followButton)
    
    var handleUnFollowTappedClosure: ((String) -> ())?
    var handleFollowTappedClosure: ((String) -> ())?
    var handlePostTappedClosure: ((String) -> ())?
    var handleProfileImageViewClosure: ((String) -> ())?
    
    var notificationCell: Notification?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureUI()
        configureLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureUI() {
        backgroundColor = .white
        selectionStyle = .none
        addSubview(profileImageView)
        [infoLabel , followButton, postImageView].forEach{ contentView.addSubview($0)}
        profileImageView.centerY(inView: self, leftAnchor: leftAnchor, paddingLeft: CGFloat(StringRepresentationOfDigit.twelve))
        
        followButton.centerY(inView: self)
        followButton.anchor(right: rightAnchor, paddingRight: CGFloat(StringRepresentationOfDigit.twelve))
        postImageView.centerY(inView: self)
        postImageView.anchor(right: rightAnchor, paddingRight: CGFloat(StringRepresentationOfDigit.twelve))
        infoLabel.centerY(inView: profileImageView, leftAnchor: profileImageView.rightAnchor, paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
        infoLabel.anchor(right: followButton.leftAnchor, paddingRight: CGFloat(StringRepresentationOfDigit.four))
        
        BorderLayer.instantiate(view: self, lineWidth: Constants.lineWidth, strokeColor: .lineConnecting, borders: .top)
    }
    
    private func configureLabel() {
        followButton.addTarget(self, action: #selector(handleFollowTapped), for: .touchUpInside)
        let tap =  UITapGestureRecognizer(target: self, action: #selector(handlePostTapped))
        postImageView.addGestureRecognizer(tap)
    }

    @objc
    func handleFollowTapped() {
        guard let notificationCell = notificationCell else { return }
        if notificationCell.userIsFollowed {
            handleUnFollowTappedClosure?(notificationCell.uid)
        } else {
            handleFollowTappedClosure?(notificationCell.uid)
        }
    }
    
    @objc
    func handlePostTapped() {
        guard let unwrappedNotifications = notificationCell?.postId else { return }
        handlePostTappedClosure?(unwrappedNotifications)
        print("DEBUG: post tapped")
    }
    
//    func notificationMessage(username: String, message: String) -> NSAttributedString {
//        let attributedText = NSMutableAttributedString(string: username, attributes: [.font: UIFont.font(with: .bold, size: .small)])
//        attributedText.append(NSAttributedString(string: message, attributes: [.font: UIFont.font(with: .regular, size: .small)]))
//        attributedText.append(NSAttributedString(string: " 2m", attributes: [.font: UIFont.font(with: .regular, size: .small), .foregroundColor: UIColor.lightGray]))
//        return attributedText
//    }
    
    func update(_ notification: Notification) {
        self.notificationCell = notification
        guard let profileImageUrl = URL(string: notification.userProfileImageUrl) else { return }
        guard let postImageUrl = URL(string: notification.postImageUrl ?? .empty) else { return }
        
        profileImageView.sd_setImage(with: profileImageUrl)
        postImageView.sd_setImage(with: postImageUrl)
        infoLabel.attributedText = NSAttributedString(attributedString: infoLabel.notificationMessages(username: notification.username, message: notification.type.notificationMessage, time: notification.timestampString!))
        
        followButton.isHidden = !notification.shouldHidePostImage
        postImageView.isHidden = notification.shouldHidePostImage
        followButton.setTitle(notification.followButtonText, for: .normal)
        followButton.backgroundColor = notification.followButtonBackgroundColor
        followButton.setTitleColor(notification.followButtonTetxColor, for: .normal)
        
       //infoLabel.attributedText = notificationMessage(username: notification.username, message: notification.type.notificationMessage)
    }
}
