//
//  NotificationDataSourceItem.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

struct NotificationDataSourceItem: DataSourceIdentifiable & Equatable & Hashable {
    let identity: Int
    let viewIdentifier: String?
    let title: String?
    let imageView: UIImage? = nil
    let text: String?
    let style: Style
    let cellType: NotificationCellType
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(viewIdentifier)
    }
    
    static func == (lhs: NotificationDataSourceItem, rhs: NotificationDataSourceItem) -> Bool {
        return lhs.identity == rhs.identity
    }
    
    enum Style {
        case icon(IconType, UIImage? = nil)
        enum IconType {
            case post
            case comment
        }
    }
}

enum NotificationCellType: CaseIterable {
    case post
    
    var style: NotificationDataSourceItem.Style {
        switch self {
        case .post:
            return .icon(.post, #imageLiteral(resourceName: "venom-7"))
        }
    }
    
    var title: String {
        switch self {
        case .post:
            return "Post"
        }
    }
}
