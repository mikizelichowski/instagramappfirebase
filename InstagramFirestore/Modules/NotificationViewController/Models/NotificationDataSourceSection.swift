//
//  NotificationDataSourceSection.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

struct NotificationDataSourceSection: DataSourceSection {
    let identity: String
    let viewIdentifier: String?
    let title: String? = nil
    let items: [NotificationDataSourceItem]
    let style: NotificationCellType?
    
}
