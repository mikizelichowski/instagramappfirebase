//
//  NotificationViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class NotificationViewController: UIViewController {
    private enum Constants {
        static let rowHeight: CGFloat = 80.0
    }
    
    private let tableView = UITableView()
    private let refresher = UIRefreshControl()
    
    private var viewModel: NotificationViewModelProtocol
    
    init(with viewModel: NotificationViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super .viewDidLoad()
        
        configureTableView()
        viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation(false, title: viewModel.title)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNavigation(false)
    }
    
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerTableViewCell(NotificationCell.self)
        tableView.rowHeight = Constants.rowHeight
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        setupLayout()
        tableView.refreshControl = refresher
        refresher.addTarget(self, action: #selector(handleRefresh),
                            for: .valueChanged)
    }
    
    func setupLayout() {
        view.addSubview(tableView)
        tableView.fillSuperViewArea()
    }
    
    @objc
    func handleRefresh() {
        viewModel.refresher()
        refresher.endRefreshing()
    }
}

extension NotificationViewController: NotificationViewModelDelegate {
    func reloadDataTableView() {
        tableView.reloadData()
    }
    
    func showFeedController(_ controller: FeedViewController) {
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showProfileController(_ controller: ProfileViewController) {
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showLoading(_ state: Bool) {
        showLoader(state)
    }
}

extension NotificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.coordinatorProfile(at: indexPath)
    }
}

extension NotificationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSourceCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotificationCell = tableView.dequeueReusableTablewViewCell(indexPath: indexPath)
        
        cell.handlePostTappedClosure = { [weak self] post in
            self?.viewModel.wantsToViewPost(post)
        }
        
        cell.handleFollowTappedClosure = { [weak self] follow in
            self?.viewModel.wantsToFollow(follow)
        }
        
        cell.handleUnFollowTappedClosure = { [weak self] unFollow in
            self?.viewModel.wantsToUnfollow(unFollow)
        }
        
        cell.update(viewModel.item(at: indexPath))
        return cell
    }
}







//
//final class NotificationViewController: UIViewController {
//    private var collectionView: UICollectionView = {
//        let layout = UICollectionViewFlowLayout()
//        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        return cv
//    }()
//
//
//    private var viewModel: NotificationViewModelProtocol
//
//    private var dataSource: CollectionViewDataSource<NotificationDataSourceSection,NotificationDataSourceItem>!
//
//    init(with viewModel: NotificationViewModelProtocol) {
//        self.viewModel = viewModel
//        super.init(nibName: nil, bundle: nil)
//        self.viewModel.delegate = self
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        view.backgroundColor = .systemBlue
//        configureUI()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        setupNavigation(false)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        setupNavigation(false, title: .empty)
//    }
//
//    private func configureUI() {
//        view.addSubview(collectionView)
//        collectionView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
//                              left: view.safeAreaLayoutGuide.leftAnchor,
//                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
//                              right: view.safeAreaLayoutGuide.rightAnchor)
//        collectionView.backgroundColor = .white
//        collectionView.registerCell(FeedCell.self)
//    }
//
//    private func setupDataSource() {
//        dataSource = CollectionViewDataSource(collectionView, cellConfigureClosure: { cell, model -> (UICollectionViewCell) in
//            switch cell {
//            case let notificationCell as NotificationCell:
//                notificationCell.model = model
//            default: assertionFailure("Unsupported cell type \(type(of: cell))")
//            }
//            return cell
//        })
//
//        dataSource.cellDidTapClosureType = { [weak self] item, _, _ in
//            self?.viewModel.didTapCollectionViewCell(item)
//        }
//    }
//}
//
//extension NotificationViewController: NotificationViewModelDelegate {
//    func updateCollectionDataSource(_ dataSource: [NotificationDataSourceSection]) {
//        self.dataSource.update(with: dataSource)
//    }
//
//
//}
