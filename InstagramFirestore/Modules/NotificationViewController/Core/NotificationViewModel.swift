//
//  NotificationViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol NotificationViewModelProtocol: class {
    var delegate: NotificationViewModelDelegate! { get set }
    
    var title: String { get }
    var dataSourceCount: Int { get }
    
    func onViewDidLoad()
    func fetchNotifications()
    func item(at indexPath: IndexPath) -> Notification
    func coordinatorProfile(at indexPath: IndexPath)
    func refresher()
    
    func wantsToFollow(_ uid: String)
    func wantsToUnfollow(_ uid: String)
    func wantsToViewPost(_ postId: String)
}

protocol NotificationViewModelDelegate: class {
    func reloadDataTableView()
    func showFeedController(_ controller: FeedViewController)
    func showProfileController(_ controller: ProfileViewController)
    func showLoading(_ state: Bool)
}

final class NotificationViewModel {
    weak var delegate: NotificationViewModelDelegate!
    
    var post: Post?
    var notification: Notification?
    private var notifications: [Notification] = []
}

extension NotificationViewModel: NotificationViewModelProtocol {
    var title: String { return "Notifications" }
    var dataSourceCount: Int { return notifications.count}
    
    func onViewDidLoad() {
        fetchNotifications()
    }
    
    func item(at indexPath: IndexPath) -> Notification {
        return notifications[indexPath.row]
    }
    
    func refresher() {
        notifications.removeAll()
        fetchNotifications()
    }
    
    func fetchNotifications() {
        NotificationService.fetchNotification { result in
            switch result {
            case .success(let notifications):
                self.notifications = notifications
                self.checkIfUserIsFollowed()
                self.delegate.reloadDataTableView()
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    func checkIfUserIsFollowed() {
        notifications.forEach { notif in
            guard notif.type == .follow else { return }
            UserService.checkIfUserIsFollowed(uid: notif.uid) { isFollowed in
                if let index = self.notifications.firstIndex(where: {$0.id == notif.id }) {
                    self.notifications[index].userIsFollowed = isFollowed
                }
            }
        }
    }
    
    func coordinatorProfile(at indexPath: IndexPath) {
        delegate.showLoading(true)
        UserService.fetchUserData(withUid: item(at: indexPath).uid) { result in
            switch result {
            case .success(let user):
                self.delegate?.showLoading(false)
                let viewModel = ProfileViewModel(layout: UICollectionViewFlowLayout(), user: user)
                let controller = ProfileViewController(with: viewModel)
                self.delegate?.showProfileController(controller)
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    func wantsToFollow(_ uid: String) {
        delegate.showLoading(true)
        UserService.follow(uid: uid) { _ in
            self.delegate.showLoading(false)
            self.notification?.userIsFollowed.toggle()
            print("DEBUG: Follow user here..")
        }
    }
    
    func wantsToUnfollow(_ uid: String) {
        delegate.showLoading(true)
        UserService.unfollow(uid: uid) { _ in
            self.delegate.showLoading(false)
            self.notification?.userIsFollowed.toggle()
            print("DEBUG: Unfollow user here")
        }
    }
    
    func wantsToViewPost(_ postId: String) {
        delegate.showLoading(true)
        PostService.fetchSignlePost(withPostId: postId) { result in
            switch result {
            case .success(let post):
                self.delegate.showLoading(false)
                let feedModel = FeedViewModel(layout: UICollectionViewFlowLayout())
                let controller = FeedViewController(with: feedModel)
                controller.post = post
                self.delegate?.showFeedController(controller)
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
}

