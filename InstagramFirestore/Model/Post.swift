//
//  Post.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Firebase

struct Post {
    var caption: String
    var likes: Int
    let imageUrl: String
    let ownerUid: String
    let timestamp: Timestamp
    let postId: String
    let ownerUserImageUrl: String
    let ownerUsername: String
    var didLike = false
    
    init(postId: String, dictionary: [String: Any]) {
        self.postId = postId
        self.caption = dictionary["caption"] as? String ?? .empty
        self.likes = dictionary["likes"] as? Int ?? 0
        self.imageUrl = dictionary["imageUrl"] as? String ?? .empty
        self.ownerUid = dictionary["ownerUid"] as? String ?? .empty
        self.timestamp = dictionary["timestamp"] as? Timestamp ?? Timestamp(date: Date())
        self.ownerUserImageUrl = dictionary["ownerUserImageUrl"] as? String ?? .empty
        self.ownerUsername = dictionary["ownerUsername"] as? String ?? .empty
    }

    var likesLabelText: String { likes != 1 ? "\(likes) likes" : "\(likes) like"}
    var likeButtonTintColor: UIColor { return didLike ? .red : .black}
    var likeButtonImage: UIImage { didLike ? Asset.like_selected.image : Asset.like_unselected.image }
    var timestampString: String? {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.second, .minute, .hour, .day, .weekOfMonth]
        formatter.unitsStyle = .full
        formatter.maximumUnitCount = 1
        return formatter.string(from: timestamp.dateValue(), to: Date())
    }
}
