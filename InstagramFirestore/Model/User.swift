//
//  User.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 03/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation
import Firebase

struct User: Hashable {
    let email: String
    let fullname: String
    let profileImageUrl: String
    let username: String
    let uid: String
    
    var isFollowed = false
    var stats: UserStats!
    
    var isCurrentUser: Bool { return Auth.auth().currentUser?.uid == uid}
    
    init(dictionary: [String: Any]) {
        self.email = dictionary["email"] as? String ?? .empty
        self.fullname = dictionary["fullname"] as? String ?? .empty
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? .empty
        self.username = dictionary["username"] as? String ?? .empty
        self.uid = dictionary["uid"] as? String ?? .empty
        
        self.stats = UserStats(followers: 0, following: 0, posts: 0)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(uid)
    }
    
    static func == (lhs: User, rhs: User) -> Bool {
        lhs.uid == rhs.uid
    }
}

struct UserStats: Hashable {
    let followers: Int
    let following: Int
    let posts: Int
}
