//
//  Comment.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 27/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Firebase

struct Comment {
    let uid: String
    let username: String
    let profileImageUrl: String
    let timestamp: Timestamp
    let commentText: String
    
    init(dictionary: [String: Any]) {
        self.uid = dictionary["uid"] as? String ?? .empty
        self.username = dictionary["username"] as? String ?? .empty
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? .empty
        self.timestamp = dictionary["timestamp"] as? Timestamp ?? Timestamp(date: Date())
        self.commentText = dictionary["comment"] as? String ?? .empty
    }
}
