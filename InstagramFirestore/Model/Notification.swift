//
//  Notification.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 30/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Firebase

enum NotificationType: Int {
    case like
    case follow
    case comment
    
    var notificationMessage: String {
        switch self {
        case .like: return " liked your post"
        case .follow: return " started following you"
        case .comment: return " commented on your post"
        }
    }
}

struct Notification {
    let uid: String
    var postImageUrl: String?
    var postId: String?
    let timestamp: Timestamp
    let type: NotificationType
    let id: String
    let userProfileImageUrl: String
    let username: String
    var userIsFollowed = false
    
    init(dictionary: [String: Any]) {
        self.timestamp = dictionary["timestamp"] as? Timestamp ?? Timestamp(date: Date())
        self.id = dictionary["id"] as? String ?? .empty
        self.uid = dictionary["uid"] as? String ?? .empty
        self.postId = dictionary["postId"] as? String ?? .empty
        self.postImageUrl = dictionary["postImageUrl"] as? String ?? .empty
        self.type = NotificationType(rawValue: dictionary["type"] as? Int ?? 0) ?? .like
        self.userProfileImageUrl = dictionary["userProfileImageUrl"] as? String ?? .empty
        self.username = dictionary["username"] as? String ?? .empty
    }
    
    var shouldHidePostImage: Bool {
        return self.type == .follow }
    
    var followButtonText: String {
        return self.userIsFollowed ? "Following" : "Follow"}
    
    var followButtonBackgroundColor: UIColor {
        return userIsFollowed ? .white : .systemBlue }
    
    var followButtonTetxColor: UIColor {
        return userIsFollowed ? .black : .white
    }
    
    var timestampString: String? {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.second, .minute, .hour, .day, .weekOfMonth]
        formatter.maximumUnitCount = 1
        formatter.unitsStyle = .abbreviated
        return formatter.string(from: timestamp.dateValue(), to: Date())
    }
}
