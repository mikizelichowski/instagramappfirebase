//
//  UserService.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 03/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Firebase

typealias FirestoreCompletion = (Error?) -> Void

struct UserService {
    private enum Constants {
        static let userfollowing = "user-following"
        static let userfollowers = "user-followers"
    }
    
    static func fetchUserData(withUid uid: String, completion: @escaping(Result<User, Error>) -> ()){
        COLLECTION_USERS.document(uid).getDocument { snapshot, error in
            guard let dictionary = snapshot?.data() else { return }
            let user = User(dictionary: dictionary)
            userID = Auth.auth().currentUser?.uid
            completion(.success(user))
        }
    }
    
    static func fetchUsersData(_ completion: @escaping(Result<[User], Error>) -> ()) {
        COLLECTION_USERS.getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else { return }
            let users = snapshot.documents.map({ User(dictionary: $0.data())})
            userID = Auth.auth().currentUser?.uid
            completion(.success(users))
        }
    }
    
    static func follow(uid: String, completion: @escaping(FirestoreCompletion)) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        COLLECTION_FOLLOWING.document(currentUid).collection(Constants.userfollowing).document(uid)
            .setData([:]) { error in
                COLLECTION_FOLLOWERS.document(uid).collection(Constants.userfollowers).document(currentUid).setData([:], completion: completion)
            }
    }
    
    static func unfollow(uid: String, completion: @escaping(FirestoreCompletion)) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        COLLECTION_FOLLOWING.document(currentUid).collection(Constants.userfollowing).document(uid).delete { error in
            COLLECTION_FOLLOWERS.document(uid).collection(Constants.userfollowers)
                .document(currentUid).delete(completion: completion)
        }
    }
    
    static func checkIfUserIsFollowed(uid: String, completion: @escaping(Bool) -> Void) {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        
        COLLECTION_FOLLOWING.document(currentUid).collection(Constants.userfollowing).document(uid).getDocument { (snapshot, error) in
            guard let isFollowed = snapshot?.exists else { return }
            completion(isFollowed)
        }
    }
    
    static func fetchUserStats(uid: String, completion: @escaping(UserStats) -> Void) {
        COLLECTION_FOLLOWERS.document(uid).collection(Constants.userfollowers).getDocuments { (snapshot, _)
            in
            let followers = snapshot?.documents.count ?? 0
            
            COLLECTION_FOLLOWING.document(uid).collection(Constants.userfollowing).getDocuments { (snapshot, _)
                in
                let following = snapshot?.documents.count ?? 0
                COLLECTION_POSTS.whereField("ownerUid", isEqualTo: uid).getDocuments { (snapshot, _) in
                    let posts = snapshot?.documents.count ?? 0
                    completion(UserStats(followers: followers, following: following, posts: posts))
                }
            }
        }
    }
    
    
    // ----------------------------------------------
//    // MARK: - Old option without completion(.success)
//    static func fetchUsers(completion: @escaping([User]) -> Void) {
//        COLLECTION_USERS.getDocuments { (snapshot, error) in
//            guard let snapshot = snapshot else { return }
//            let users = snapshot.documents.map({ User(dictionary: $0.data()) })
//            completion(users)
//            userID = Auth.auth().currentUser?.uid
//        }
//    }
//    static func fetchUser(completion: @escaping(User) -> Void) {
//        guard let uid = Auth.auth().currentUser?.uid else { return }
//        COLLECTION_USERS.document(uid).getDocument { snapshot, error in
//            guard let dictionary = snapshot?.data() else { return }
//            let user = User(dictionary: dictionary)
//            completion(user)
//            userID = Auth.auth().currentUser?.uid
//        }
//    }
}
