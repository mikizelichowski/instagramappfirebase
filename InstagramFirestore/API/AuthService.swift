//
//  AuthService.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 28/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

struct AuthCredentials {
    let email: String
    let password: String
    let fullname: String
    let username: String
    let profileImage: UIImage
}

var userID: String?

struct AuthService {
    
    static func logout() {
        do {
            try Auth.auth().signOut()
            userID = nil
        } catch {
            print("DEBUG: Error signig out.. \(error.localizedDescription)")
        }
    }
    
    static func logUserIn(withEmail email: String, password: String, completion: AuthDataResultCallback?) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            userID = Auth.auth().currentUser?.uid
            completion?(result, error)
        }
//        Auth.auth().signIn(withEmail: email, password: password, completion: completion)
//        userID = Auth.auth().currentUser?.uid
    }
    
    static func registerUser(withCredential credentials: AuthCredentials, completion: @escaping(Error?) -> Void){
        ImageUploader.uploadImage(image: credentials.profileImage) { imageUrl in
            Auth.auth().createUser(withEmail: credentials.email, password: credentials.password) { (result, error) in
                if let error = error {
                    print("DEBUG: Failed to register user \(error.localizedDescription)")
                    return
                }
                guard let uid = result?.user.uid else { return }
                let data: [String: Any] = ["email": credentials.email,
                                           "fullname": credentials.fullname,
                                           "username": credentials.username,
                                           "profileImageUrl": imageUrl,
                                           "uid": uid]
               COLLECTION_USERS.document(uid).setData(data, completion: completion)
                userID = Auth.auth().currentUser?.uid
            }
        }
    }
    
    static func resetPassword(withEmail email: String, completion: SendPasswordResetCallback?) {
        Auth.auth().sendPasswordReset(withEmail: email, completion: completion)
    }
}
