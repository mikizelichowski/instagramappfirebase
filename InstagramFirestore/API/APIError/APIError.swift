//
//  APIError.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 14/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

enum APIError: String, Error {
    case serverUnavailable = "Server is unavailable"
    case permissionDenied = "Permission denied"
}
