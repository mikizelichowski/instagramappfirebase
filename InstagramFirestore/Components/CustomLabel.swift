//
//  CustomLabel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

enum LabelStyle: CaseIterable {
    case defaultLabel
    case profileLabel
    case postLabel
    case lightLabel
    case commentLabel
}

final class CustomLabel: UILabel {
    
    init(style: LabelStyle, title: String? = nil) {
        super.init(frame: .zero)
        styleLabel(style, title: title)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var styleAppearance: LabelStyle = .defaultLabel
    
    private func styleLabel(_ styles: LabelStyle, title: String? = nil, value: Int? = nil) {
        switch styles {
        case .defaultLabel:
            textColor = .black
            textAlignment = .center
            numberOfLines = .zero
            text = title
            font = .font(with: .medium, size: .small)
        case .profileLabel:
            text = title
            numberOfLines = .zero
            textAlignment = .center
            textColor = .black
        case .postLabel:
            numberOfLines = .zero
            textColor = .black
            text = title
            font = .font(with: .bold, size: .small)
        case .lightLabel:
            numberOfLines = .zero
            text = title
            font = .font(with: .bold, size: .smallerMedium)
            textColor = .lightGray
        case .commentLabel:
            textColor = .black
            numberOfLines = .zero
            text = title
            font = .font(with: .bold, size: .small)
        }
    }
    
    func notificationMessages(username: String, message: String, time: String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: username, attributes: [.font: UIFont.font(with: .bold, size: .small)])
        attributedText.append(NSAttributedString(string: message, attributes: [.font: UIFont.font(with: .regular, size: .small)]))
        attributedText.append(NSAttributedString(string: " \(time)", attributes: [.font: UIFont.font(with: .regular, size: .small), .foregroundColor: UIColor.lightGray]))
        return attributedText
    }
    
    func attributedString(_ title: String, font: UIFont? = nil, secondTitle: String, secondFont: UIFont? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: title, attributes: [.font: font!])
        attributedString.append(NSAttributedString(string: secondTitle, attributes: [.font: secondFont!]))
        return attributedString
    }
    
    func changeLabel(_ styles: LabelStyle, title: String? = nil, colorTxt: UIColor, Size: CGFloat) {
        switch styles {
        case .defaultLabel:
            styleAppearance = styles
        case .profileLabel:
            ()
        case .postLabel:
            ()
        case .lightLabel:
            ()
        case .commentLabel: ()
        }
    }
        
    func updateLabel(_ titleText: String? = .empty, valueText: String? = .empty, style: LabelStyle? = .defaultLabel, color: UIColor? = nil) {
        switch style {
        case .defaultLabel: ()
        case .postLabel: ()
        case .profileLabel: ()
        case .lightLabel:
            textColor = color
        case .commentLabel: ()
        case .none: ()
        }
    }
}
