//
//  RoundedButton.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 25/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class RoundedButton: UIButton {
    enum Style {
        case light
        case register
        case login
        case acceptBottom
        case cancelBottom
    }
    
    private enum Constans {
        enum Margin {
            static let normal: CGFloat = 12.0
            static let small: CGFloat = 6.0
        }
        static let alphaBackgroundColor: CGFloat = 0.5
        static let cornerRadius: CGFloat = 5.0
        static let borderWidth: CGFloat = 0.5
        static let padding: CGFloat = 6.0
        static let titleSpacing: CGFloat = 4.0
        static let highLighted: CGFloat = 0.7
        static let cancelButtonAlpha: CGFloat = 0.7
        static let setHeight: CGFloat = 50.0
    }
    
    private var appearanceStyle: Style = .light
    
    override var isEnabled: Bool {
        set {
            super.isEnabled = newValue
            alpha = newValue ? 1.0 : 0.5
        } get {
            return super.isEnabled
        }
    }
    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//       
//    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
         setupAppearance()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switchStyle()
    }
    
    func setup(title: String, style: Style = .light, image: UIImage? = nil) {
        setTitle(title, for: .normal)
        setImage(image, for: .normal)
        appearanceStyle = style
        setupInsets()
        setupAppearance()
    }
    
    func setupInsets() {
        titleEdgeInsets = .zero
        imageEdgeInsets = UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: Constans.Margin.small)
    }
    
    func setupAppearance() {
        clipsToBounds = true
        layer.cornerRadius = Constans.cornerRadius
        layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        layer.borderWidth = .zero
        titleLabel?.font = .font(with: .bold, size: .smallMedium)
        tintColor = .white
    }
    
    private func switchStyle() {
        switch appearanceStyle {
        case .light:
            setTitleColor(UIColor.systemBlue.withAlphaComponent(Constans.highLighted), for: .highlighted)
            backgroundColor = .white
            layer.borderWidth = Constans.borderWidth
            imageView?.tintColor = .blue
        case .register:
            setTitleColor(.white, for: .normal)
            backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(Constans.alphaBackgroundColor)
            layer.cornerRadius = Constans.cornerRadius
            setHeight(Constans.setHeight)
            titleLabel?.font = .font(with: .bold, size: .big)
        case .login:
            setTitleColor(.white, for: .normal)
            backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(Constans.alphaBackgroundColor)
            layer.cornerRadius = Constans.cornerRadius
            setHeight(Constans.setHeight)
            titleLabel?.font = .font(with: .bold, size: .big)
        case .acceptBottom:
            setTitleColor(.white, for: .normal)
            backgroundColor = .systemBlue
            layer.borderWidth = Constans.borderWidth
            titleLabel?.font = .font(with: .regular, size: .smallMedium)
        case .cancelBottom:
            setTitleColor(.white, for: .normal)
            backgroundColor = UIColor.systemBlue.withAlphaComponent(Constans.cancelButtonAlpha)
            layer.borderWidth = Constans.borderWidth
            titleLabel?.font = .font(with: .regular, size: .smallMedium)
        }
    }
}
