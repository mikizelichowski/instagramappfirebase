//
//  CustomButton.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 23/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

enum ButtonStyle: Int, CaseIterable {
    case customButton
    case iconButton
    case iconButtonSelect
    case editButton
    case usernameButton
    case postButton
    case commentButton
    case followButton
}

final class CustomButton: UIButton {
    private enum Constants {
        static let backgroundAlpha: CGFloat = 0.5
        static let imageAlpha: CGFloat = 0.2
        static let height: CGFloat = 50.0
        static let cornerRadius: CGFloat = 5.0
        static let borderWidth: CGFloat = 0.5
        static let followButtonHeight: CGFloat = 32.0
    }
    
    init(title: String? = nil, style: ButtonStyle, image: UIImage? = nil) {
        super.init(frame: .zero)
        styles(style, title: title, image: image)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateLikesButton(_ stateLike: Bool) {
        if stateLike == false {
            setImage(Asset.like_unselected.image, for: .normal)
        } else {
            setImage(Asset.like_selected.image, for: .normal)
            tintColor = .red
        }
    }
    
    func updateButton(_ title: String, state: Bool){
        isHidden = state
        setTitle(title, for: .normal)
    }
    
    private func styles(_ style: ButtonStyle, title: String? = nil, image: UIImage? = nil) {
        switch style {
        case .customButton:
            setTitle(title, for: .normal)
            setTitleColor(.black, for: .normal)
            backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(Constants.backgroundAlpha)
            layer.cornerRadius = Constants.cornerRadius
            setHeight(Constants.height)
            titleLabel?.font = .font(with: .bold, size: .big)
        case .iconButton:
            setImage(image, for: .normal)
        case .iconButtonSelect:
            setImage(image?.withTintColor(UIColor(white: .zero, alpha: Constants.imageAlpha)), for: .normal)
        case .editButton:
            setTitle(title, for: .normal)
            layer.cornerRadius = Constants.cornerRadius
            layer.borderColor = UIColor.lightGray.cgColor
            layer.borderWidth = Constants.borderWidth
            titleLabel?.font = .font(with: .bold, size: .smallMedium)
            setTitleColor(.black, for: .normal)
            layer.addShadow(type: .profileButton)
        case .usernameButton:
            setTitle(title, for: .normal)
            setTitleColor(.black, for: .normal)
            titleLabel?.font = .font(with: .bold, size: .smallerMedium)
        case .postButton:
            setImage(image?.withTintColor(.black), for: .normal)
        case .commentButton:
            setTitle(title, for: .normal)
            setTitleColor(.black, for: .normal)
            titleLabel?.font = .font(with: .bold, size: .small)
        case .followButton:
            setTitle(title, for: .normal)
            setTitleColor(.black, for: .normal)
            titleLabel?.font = .font(with: .bold, size: .small)
            layer.addShadow(type: .profileButton)
            layer.cornerRadius = Constants.cornerRadius
            layer.borderColor = UIColor.lightGray.cgColor
            layer.borderWidth = Constants.borderWidth
            setDimensions(height: Constants.followButtonHeight, width: 88)
        }
    }
}
