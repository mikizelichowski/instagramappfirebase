//
//  CommentInputAccessoryView.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 25/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol CommentInputAccessoryViewDelegate: class {
    func inputView(_ inputView: CommentInputAccessoryView, wantsToUploadComment comment: String)
}

final class CommentInputAccessoryView: UIView {
    private enum Constants {
        static let lineWidth: CGFloat = 0.5
        static let heightButton: CGFloat = 50.0
    }
    
    weak var delegate: CommentInputAccessoryViewDelegate?
    
    private let commentTextView = InputTextView()
    private let postButton = CustomButton(title: Localized.CommentView.Button.post, style: .commentButton)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        backgroundColor = .white
        autoresizingMask = .flexibleHeight
        [commentTextView, postButton].forEach { addSubview($0)}
        BorderLayer.instantiate(view: self, lineWidth: Constants.lineWidth, strokeColor: .lineConnecting, borders: .top)
        postButton.anchor(top: topAnchor,
                          right: rightAnchor,
                          paddingRight: CGFloat(StringRepresentationOfDigit.eight))
        postButton.setDimensions(height: Constants.heightButton, width: Constants.heightButton)
        commentTextView.anchor(top: topAnchor,
                               left: leftAnchor,
                               bottom: safeAreaLayoutGuide.bottomAnchor,
                               right: postButton.leftAnchor,
                               paddingTop: CGFloat(StringRepresentationOfDigit.eight),
                               paddingLeft: CGFloat(StringRepresentationOfDigit.eight),
                               paddingBottom: CGFloat(StringRepresentationOfDigit.eight),
                               paddingRight: CGFloat(StringRepresentationOfDigit.eight))
        setupLabel()
    }
    
    func setupLabel() {
        commentTextView.setupText(.commentPost)
        commentTextView.placeholderShouldCenter = true
        postButton.addTarget(self, action: #selector(handlePostTapped), for: .touchUpInside)
    }
    
    // this means will figure out the size based on the view, the dimensions of the view components inside
    override var intrinsicContentSize: CGSize {
        return .zero
    }
    
    @objc
    func handlePostTapped() {
        delegate?.inputView(self, wantsToUploadComment: commentTextView.text)
    }
    
    func clearCommentTextView() {
        commentTextView.text = nil
        commentTextView.placeholderLabel.isHidden = false
    }
}
