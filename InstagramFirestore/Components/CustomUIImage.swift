//
//  CustomUIImage.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

enum ImageStyle: CaseIterable {
    case profile
    case smallProfile
    case post
    case smallProfileComments
    case notification
}

final class CustomUIImage: UIImageView {
    private enum Constants {
        static let magicNumberEighty: CGFloat = 80.0
        static let magicNumberTwelve: CGFloat = 12.0
        static let magicNumberFifteen: CGFloat = 15.0
        static let magicNumberSixteen: CGFloat = 16.0
        static let magicNumberTwentyFour: CGFloat = 24.0
        static let magicNumberSixty: CGFloat = 60.0
        static let StackHeight: CGFloat = 50.0
        static let lineWidth: CGFloat = 0.5
    }
    
    init(style: ImageStyle, image: UIImage? = nil, width: CGFloat? = .zero, height: CGFloat? = .zero ) {
        super.init(frame: .zero)
        styles(style, image: image, width: width, height: height)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(_ style: ImageStyle) {
        switch style {
        case .profile:
            ()
        case .smallProfile:
            ()
        case .post:
            layer.cornerRadius = CGFloat(StringRepresentationOfDigit.ten)
        case .smallProfileComments:
            ()
        case .notification: ()
        }
    }
    
    private func styles(_ style: ImageStyle, image: UIImage? = nil, width: CGFloat? = .zero, height: CGFloat? = .zero) {
        switch style {
        case .profile:
            self.image = image
            backgroundColor = .lightGray
            contentMode = .scaleAspectFill
            clipsToBounds = true
            backgroundColor = .lightGray
            setDimensions(height: Constants.magicNumberEighty, width: Constants.magicNumberEighty)
            layer.cornerRadius = Constants.magicNumberEighty / CGFloat(StringRepresentationOfDigit.two)
        case .smallProfile:
            backgroundColor = .lightGray
            self.image = image
            contentMode = .scaleAspectFill
            clipsToBounds = true
            isUserInteractionEnabled = true
            setDimensions(height: Constants.magicNumberSixty, width: Constants.magicNumberSixty)
            layer.cornerRadius = Constants.magicNumberSixty / CGFloat(StringRepresentationOfDigit.two)
        case .post:
            self.image = image
            backgroundColor = .lightGray
            contentMode = .scaleAspectFill
            clipsToBounds = true
            isUserInteractionEnabled = true
            setDimensions(height: height!, width: width!)
        case .smallProfileComments:
            backgroundColor = .lightGray
            self.image = image
            contentMode = .scaleAspectFill
            clipsToBounds = true
            isUserInteractionEnabled = true
            setDimensions(height: CGFloat(StringRepresentationOfDigit.fourty), width: CGFloat(StringRepresentationOfDigit.fourty))
            layer.cornerRadius = CGFloat(StringRepresentationOfDigit.fourty) / CGFloat(StringRepresentationOfDigit.two)
        case .notification:
            backgroundColor = .lightGray
            self.image = image
            contentMode = .scaleAspectFill
            clipsToBounds = true
            isUserInteractionEnabled = true
            setDimensions(height: CGFloat(StringRepresentationOfDigit.fourty), width: CGFloat(StringRepresentationOfDigit.fourty))
        }
    }
}
