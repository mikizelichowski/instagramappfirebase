//
//  InputTextView.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 20/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

enum PlaceholderStyle {
    case commentPost
    case createPost
}

class InputTextView: UITextView {
    var placeholderText: String?  {
        didSet { placeholderLabel.text = placeholderText }
    }
    
    let placeholderLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        return label
    }()
    
    var placeholderShouldCenter: Bool = true {
        didSet {
            if placeholderShouldCenter {
                placeholderLabel.anchor(left: leftAnchor,
                                        right: rightAnchor,
                                        paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
                placeholderLabel.centerY(inView: self)
            } else {
                placeholderLabel.anchor(top: topAnchor,
                                        left: leftAnchor,
                                        paddingTop: CGFloat(StringRepresentationOfDigit.six),
                                        paddingLeft: CGFloat(StringRepresentationOfDigit.eight))
            }
        }
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
     
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        backgroundColor = .white
        addSubview(placeholderLabel)
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextDidChange), name: UITextView.textDidChangeNotification, object: nil)
    }
    
    func setupText(_ style: PlaceholderStyle) {
        switch style {
        case .commentPost:
            font = .font(with: .regular, size: .small)
            placeholderText = Localized.CommentView.Placeholder.comment
            isScrollEnabled = false
            textColor = .black
        case .createPost:
            font = .font(with: .regular, size: .small)
            placeholderText = Localized.UploadPost.Placeholder.caption
            textColor = .black
        }
    }
    
    @objc func handleTextDidChange() {
        placeholderLabel.isHidden = !text.isEmpty
    }
}
