//
//  InputView.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 25/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class InputView: UIView {
    private enum Constants {
        //static let infoImage = UIImage(systemName: "info.circle")
        static let setHeightTextField: CGFloat = 46.0
    }
    
    private let contentView = UIView()
    private let containerStackView = UIStackView()
    private let infoLabel = UILabel()
    private let errorLabel = UILabel()
    private let hintStackView = UIStackView()
    private let hintIconImageView = UIImageView()
    private let hintLabel = UILabel()
    private let textField: CustomText = {
        let tf = CustomText()
        tf.setHeight(Constants.setHeightTextField)
        return tf
    }()
    
    var isEmptyClosure: ((InputView.InputType, Bool) -> ())?
    
    var text: String? {
        set {
            textField.update(text: newValue)
        }
        get {
            return textField.text
        }
    }
    
    var errorMessage: String? {
        didSet {
            errorLabel.text = errorMessage
            errorLabel.isHidden = errorMessage == nil
            textField.isError = errorMessage != nil
            infoLabel.textColor = errorMessage == nil ? .blueDark : .redColor
            showHintIfIneeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLayout() {
        contentView.backgroundColor = .clear
        contentView.setWidth(UIScreen.main.bounds.width - 60)
        addSubview(contentView)
        contentView.addSubview(containerStackView)
        containerStackView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor)
        
        [infoLabel, textField].forEach { containerStackView.addArrangedSubview($0)}
        containerStackView.axis = .vertical
        containerStackView.spacing = 5
        
        [hintIconImageView, hintLabel].forEach { hintStackView.addArrangedSubview(($0))}
        hintStackView.axis = .horizontal
        hintStackView.spacing = 5
        hintIconImageView.setDimensions(height: 16, width: 16)
    }
    
    func update(renderable: Renderable) {
        infoLabel.text = renderable.title
        textField.placeholderText = renderable.placeholder
        textField.isSecureMode = renderable.isSecure
        hintLabel.text = renderable.hint
        showHintIfIneeded()
        textField.textDidChangeClosure = { [weak self] text in self?.isEmptyClosure?(renderable.type, text.isEmpty)}
    }
    
    private func setup() {
        infoLabel.textColor = .blueDark
        infoLabel.font = .font(with: .bold, size: .smallMedium)
        errorLabel.textColor = .redColor
        errorLabel.font = .font(with: .regular, size: .small)
        errorMessage = nil
        hintLabel.font = .font(with: .regular, size: .small)
        hintIconImageView.image = Asset.infoImage.image
        hintIconImageView.tintColor = .greyBlueLight
        textField.borderColorHasChangedClosure = changeInfoLabel
    }
    
    private func showHintIfIneeded() {
        hintStackView.isHidden = errorMessage != nil || hintLabel.text == nil
    }
    
    private func changeInfoLabel(color: UIColor) {
        infoLabel.textColor = color
        errorLabel.text = nil
    }
}

extension InputView {
    enum InputType {
        case email
        case password
        case username
        case fullname
    }
    
    struct Renderable {
        let type: InputType
        let title: String
        let placeholder: String
        let isSecure: Bool
        let hint: String?
    }
}
