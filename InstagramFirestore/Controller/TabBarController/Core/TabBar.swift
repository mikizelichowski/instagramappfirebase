//
//  TabBar.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

enum TabBarItem: Int, CaseIterable {
    case feed
    case search
    case imageSelector
    case notification
    case profile
    
    var title: String {
        switch self {
        case .feed:
            return "Feed"
        case .search:
            return "Search"
        case .imageSelector:
            return "Image"
        case .notification:
            return "Notification"
        case .profile:
            return "Profile"
        }
    }
    
    var image: UIImage {
        switch self {
        case .feed:
            return #imageLiteral(resourceName: "home_unselected")
        case .search:
            return #imageLiteral(resourceName: "search_unselected")
        case .imageSelector:
            return #imageLiteral(resourceName: "plus_unselected")
        case .notification:
            return #imageLiteral(resourceName: "send2")
        case .profile:
            return #imageLiteral(resourceName: "profile_unselected")
        }
    }
}
