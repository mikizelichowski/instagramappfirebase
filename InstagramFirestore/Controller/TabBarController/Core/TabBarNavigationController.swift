//
//  TabBarNavigationController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

enum ForStatusBarStyle {
    case feed
    case search
    case imageSelector
    case notification
    case profile
    case defaultStyle
}

final class TabBarNarvigationController:  BaseNavigationController {
    private var item: TabBarItem
    
    var statusBarStyle: ForStatusBarStyle = .defaultStyle
    
    init(item: TabBarItem) {
        self.item = item
        super.init(nibName: nil, bundle: nil)
        
        setupController()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        switch statusBarStyle {
        case .feed, .search, .imageSelector, .notification, .profile:
            return .default
        case .defaultStyle:
            return .lightContent
        }
    }
    
    private func setupController() {
        tabBarItem.title = item.title
        tabBarItem.image = item.image
    }
    
    private func setupStatusBarStyle(style: ForStatusBarStyle) {
        statusBarStyle = style
    }
}
