//
//  LoginNavigtionController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 29/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

class LoginNavigtionController: BaseNavigationController {
    
   override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationBar.backgroundColor = .white
    navigationBar.barTintColor = .white
    navigationBar.isTranslucent = true
    
    navigationBar.tintColor = .blueDark
    navigationBar.titleTextAttributes = [.font: UIFont.font(with: .bold, size: .large), .foregroundColor: UIColor.blueDark]
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
