//
//  MainTabController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 22/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase
import YPImagePicker

protocol ControllerDidFinishUploadingPostDelegate: class {
    func controllerDidFinishUploadingDelegate()
}

final class MainTabController: UITabBarController {
    private var profileViewModel: ProfileViewModelProtocol!
    
    var user: User? {
        didSet {
            guard let user = user else { return }
            configureViewControllers(withUser: user)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfUserIsLoggedIn()
        fetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        setupNavigation(true)
    }
    
    func fetch() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        UserService.fetchUserData(withUid: uid) { (result) in
            switch result {
            case .success(let user):
                self.user = user
            case .failure(let err):
                print("DEBUG: Failed fetch user data \(err.localizedDescription)")
            }
        }
    }

    func checkIfUserIsLoggedIn() {
        if Auth.auth().currentUser == nil {
            DispatchQueue.main.async {
                let viewModel = LoginViewModel()
                viewModel.authentication = self
                let controller = LoginViewController(with: viewModel)
                let nav = UINavigationController(rootViewController: controller)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    private func configureViewControllers(withUser user: User) {
        let layout = UICollectionViewFlowLayout()
        view.backgroundColor = .white
        self.delegate = self
        let feedViewModel = FeedViewModel(layout: layout)
        let feed = templateNavigationController(unselectedImage: Asset.home_unselected.image, selectedImage: Asset.home_selected.image, rootViewController: FeedViewController(with: feedViewModel))
        let searchViewModel = SearchViewModel()
        let search = templateNavigationController(unselectedImage: Asset.search_unselected.image, selectedImage: Asset.search_selected.image, rootViewController: SearchViewController(with: searchViewModel))
        let imageSelector = templateNavigationController(unselectedImage: Asset.plus_unselected.image, selectedImage: Asset.plus_unselected.image, rootViewController: ImageSelectorViewController())
//        // test for Diffable
//        let notifications = templateNavigationController(unselectedImage: Asset.like_unselected.image, selectedImage: Asset.like_selected.image, rootViewController: DataSourceController())
        
        let notificationViewModel = NotificationViewModel()
        let notifications = templateNavigationController(unselectedImage: Asset.like_unselected.image, selectedImage: Asset.like_selected.image, rootViewController: NotificationViewController(with: notificationViewModel))
        let profileViewModel = ProfileViewModel(layout: layout, user: user)
        let profile = templateNavigationController(unselectedImage: Asset.profile_unselected.image, selectedImage: Asset.profile_selected.image, rootViewController: ProfileViewController(with: profileViewModel))
        viewControllers = [feed, search, imageSelector, notifications, profile]
        tabBar.tintColor = .white
    }
    
    private func templateNavigationController(unselectedImage: UIImage, selectedImage: UIImage, rootViewController: UIViewController) -> UINavigationController {
        let nav = UINavigationController(rootViewController: rootViewController)
        nav.tabBarItem.image = unselectedImage
        nav.tabBarItem.selectedImage = selectedImage
        nav.navigationBar.tintColor = .white
        return nav
    }
    
    func didFinisPickingMedia(_ picker: YPImagePicker) {
        picker.didFinishPicking { items, _ in
            picker.dismiss(animated: false) {
                guard let selectedImage = items.singlePhoto?.image else { return }
                let viewModel = UploadPostViewModel()
                viewModel.delegatePost = self
                viewModel.currentUser = self.user
                let controller = UploadPostController(with: viewModel)
                controller.selectedImage = selectedImage
                let nav = UINavigationController(rootViewController: controller)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil
                )
            }
        }
    }
}

extension MainTabController: ControllerDidFinishUploadingPostDelegate {
    func controllerDidFinishUploadingDelegate() {
        self.dismiss(animated: true, completion: nil)
        self.selectedIndex = .zero
        guard let feedNav = viewControllers?.first as? UINavigationController else { return }
        guard let feed = feedNav.viewControllers.first as? FeedViewController else { return }
        feed.handleRefresh()
    }
}

extension MainTabController: AuthenticationDelegate {
    func authenticationDidComplete() {
        fetch()
        self.dismiss(animated: true, completion: nil)
    }
}

extension MainTabController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        let index = viewControllers?.firstIndex(of: viewController)
        if index == 2 {
            var config = YPImagePickerConfiguration()
            config.library.mediaType = .photo
            config.shouldSaveNewPicturesToAlbum = false
            config.startOnScreen = .library
            config.screens = [.library]
            config.hidesStatusBar = false
            config.hidesBottomBar = false
            config.library.maxNumberOfItems = 1 // mozemy ustawic wiecej wybranych zdjec
            
            let picker = YPImagePicker(configuration: config)
            picker.modalPresentationStyle = .fullScreen
            present(picker, animated: true, completion: nil)
            didFinisPickingMedia(picker)
        }
        return true
    }
}
