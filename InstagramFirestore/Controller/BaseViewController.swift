//
//  BaseViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 15/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
    }
}
