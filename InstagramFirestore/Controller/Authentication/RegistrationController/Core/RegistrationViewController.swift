//
//  RegistrationViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 23/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class RegistrationViewController: UIViewController {
    private let plusPhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleProfilePhotoSelect), for: .touchUpInside)
        return button
    }()
    
    private let emailInput = InputView()
    private let passwordInput = InputView()
    private let fullnameInput = InputView()
    private let usernameInput = InputView()
    private var profileImage: UIImage?
    var closureProfileImage: ((UIImage?) -> ())?
    
    private var registerButton: RoundedButton = {
        let button = RoundedButton()
        button.addTarget(self, action: #selector(handleRegisterDidTap), for: .touchUpInside)
        return button
    }()
    
    private let containerStackView = UIStackView()
    
    private let alreadyHaveAccountButton: UIButton = {
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: Localized.RegisterView.TextTitle.alreadyHaveAnAccount, secondPart: Localized.RegisterView.Button.registerButton)
        button.addTarget(self, action: #selector(handleShowLogin), for: .touchUpInside)
        return button
    }()
    
    private var viewModelAuthentication = RegistrationAuthenticationViewModel()
    private var viewModel: RegistrationViewModelProtocol
    
    init(with viewModel: RegistrationViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        configureNotificationObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        setupNavigation(true)
    }
    
    private func configureUI() {
        configureGradientLayer()
        view.addSubview(plusPhotoButton)
        plusPhotoButton.centerX(inView: view)
        plusPhotoButton.setDimensions(height: 140, width: 140)
        plusPhotoButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 32)
        registerButton.setup(title: viewModel.registerButton, style: .register)
        
        emailInput.update(renderable: viewModel.emailRenderable)
        emailInput.isEmptyClosure = viewModel.isInputEmpty
        passwordInput.update(renderable: viewModel.passwordRenderable)
        passwordInput.isEmptyClosure = viewModel.isInputEmpty
        fullnameInput.update(renderable: viewModel.fullnameRenderable)
        fullnameInput.isEmptyClosure = viewModel.isInputEmpty
        usernameInput.update(renderable: viewModel.usernameRenderable)
        usernameInput.isEmptyClosure = viewModel.isInputEmpty
        
        [containerStackView,registerButton, alreadyHaveAccountButton].forEach{view.addSubview($0)}
        [emailInput, passwordInput, fullnameInput, usernameInput].forEach { containerStackView.addArrangedSubview($0)}
        containerStackView.axis = .vertical
        containerStackView.distribution = .fillEqually
        containerStackView.setHeight(320)
        containerStackView.anchor(top:plusPhotoButton.bottomAnchor,
                                  left: view.leftAnchor,
                                  right: view.rightAnchor,
                                  paddingTop: 32,
                                  paddingLeft: 32,
                                  paddingRight: 32)
        registerButton.anchor(top: containerStackView.bottomAnchor,
                              left: view.leftAnchor,
                              right: view.rightAnchor,
                              paddingTop: 10,
                              paddingLeft: 32,
                              paddingRight: 32)
        alreadyHaveAccountButton.centerX(inView: view)
        alreadyHaveAccountButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor)
    }
    
    @objc private func handleProfilePhotoSelect() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    @objc private func handleShowLogin() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func textDidChange(sender: UITextField) {
        if sender == emailInput {
            viewModelAuthentication.email = sender.text
        } else  if sender == passwordInput {
            viewModelAuthentication.password = sender.text
        } else if sender == fullnameInput {
            viewModelAuthentication.fullname = sender.text
        } else if sender == usernameInput {
            viewModelAuthentication.username = sender.text
        }
        updateForm()
    }
    
    @objc private func handleRegisterDidTap() {
        viewModel.register(email: emailInput.text, password: passwordInput.text, fullname: fullnameInput.text, username: usernameInput.text, profileImage: profileImage)
    }
    
    private func configureNotificationObservers() {
//        emailTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
//        passwordTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
//        fullnameTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
//        usernameTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    }
}

extension RegistrationViewController: RegistrationViewModelDelegate {
    func authenticationComplete() {
        let controller = MainTabController()
        controller.authenticationDidComplete()
        let navController = UINavigationController(rootViewController: controller)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    func updateForm() {
        registerButton.backgroundColor = viewModelAuthentication.buttonBackgroundColor
        registerButton.setTitleColor(viewModelAuthentication.buttonTitleColor, for: .normal)
        registerButton.isEnabled = viewModelAuthentication.formIsValid
    }
}

extension RegistrationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.editedImage] as? UIImage else { return }
        profileImage = selectedImage
        plusPhotoButton.layer.cornerRadius = plusPhotoButton.frame.width / 2
        plusPhotoButton.layer.masksToBounds = true
        plusPhotoButton.layer.borderColor = UIColor.white.cgColor
        plusPhotoButton.setImage(selectedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        self.dismiss(animated: true, completion: nil)
    }
}
