//
//  RegistrationViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 24/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol RegistrationViewModelProtocol: class {
    var delegate: RegistrationViewModelDelegate! { get set }
    
    var emailRenderable: InputView.Renderable { get }
    var passwordRenderable: InputView.Renderable { get }
    var fullnameRenderable: InputView.Renderable { get }
    var usernameRenderable: InputView.Renderable { get }
    var registerButton: String { get }
    
    func isInputEmpty(type: InputView.InputType, isEmpty: Bool)
    func register(email: String?, password: String?, fullname: String?, username: String?, profileImage: UIImage?)
    func showFeedViewController()
}

protocol RegistrationViewModelDelegate: class {
    func updateForm()
    func authenticationComplete()
}

final class RegistrationViewModel {
    weak var delegate: RegistrationViewModelDelegate!
    private var profileImage: UIImage?
    weak var authenticationdelegate: AuthenticationDelegate?
//    private let coordinator: LoginCoordinatorProtocol
//    init(coordinator: LoginCoordinatorProtocol) {
//        self.coordinator = coordinator
//    }
    
    let emailRenderable = InputView.Renderable(type: .email, title: Localized.RegisterView.TextTitle.emailTitle, placeholder: Localized.RegisterView.Placeholder.emailPlaceholder, isSecure: false, hint: nil)
    let passwordRenderable = InputView.Renderable(type: .password, title: Localized.RegisterView.TextTitle.passwordTitle, placeholder: Localized.RegisterView.Placeholder.passwordPlaceholder, isSecure: true, hint: Localized.RegisterView.TextTitle.hintPasswordTitle)
    let fullnameRenderable = InputView.Renderable(type: .fullname, title: Localized.RegisterView.TextTitle.fullnameTitle, placeholder: Localized.RegisterView.Placeholder.fullnamePlaceholder, isSecure: false, hint: nil)
    let usernameRenderable = InputView.Renderable(type: .username, title: Localized.RegisterView.TextTitle.usernameTitle, placeholder: Localized.RegisterView.Placeholder.usernamePlaceholder, isSecure: false, hint: nil)
    
    private let inputsRequired: Set<InputView.InputType> = [.email, .password, .fullname, .username]
    private var inputs: Set<InputView.InputType> = []
}

extension RegistrationViewModel: RegistrationViewModelProtocol {
    var registerButton: String { Localized.RegisterView.Button.registerButton }
    
    func isInputEmpty(type: InputView.InputType, isEmpty: Bool) {
        if isEmpty {
            inputs.remove(type)
        } else {
            inputs.insert(type)
        }
        if inputs == inputsRequired {
            delegate.updateForm()
        }
    }
    
    func register(email: String?, password: String?, fullname: String?, username: String?, profileImage: UIImage?) {
        guard let email = email,
              let password = password,
              let fullname = fullname,
              let username = username?.lowercased(),
              let profileImage = profileImage else { return }
        let credentials = AuthCredentials(email: email, password: password, fullname: fullname, username: username, profileImage: profileImage)
        AuthService.registerUser(withCredential: credentials) { [self] error in
            if let error = error {
                print("DEBUG: Failed to register user \(error.localizedDescription)")
                return
            }
            print("DEBUG: Seuccessfully registered user with firestore..")
           // self.authenticationdelegate?.authenticationDidComplete()
            self.delegate.authenticationComplete()
        }
    }
    func showFeedViewController() {
        let controller = MainTabController()
        let navController = UINavigationController()
        navController.pushViewController(controller, animated: true)
    }
}
