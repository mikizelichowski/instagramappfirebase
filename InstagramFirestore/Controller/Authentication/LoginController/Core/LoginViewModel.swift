//
//  LoginViewModel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 24/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import Firebase

protocol AuthenticationDelegate: class {
    func authenticationDidComplete()
}

protocol LoginViewModelProtocol: class {
    var delegate: LoginViewModelDelegate! { get set }
    
    var loginButtonTitle: String { get }
    var loginRenderable: InputView.Renderable { get }
    var passwordRenderable: InputView.Renderable { get }
    
    func isInputEmpty(type: InputView.InputType, isEmpty: Bool)
    func login(email: String?, password: String?)
    func showSignUpController()
}

protocol LoginViewModelDelegate: class {
    func updateForm()
//    func updateButton(isEnabled: Bool)
}

final class LoginViewModel {
    weak var delegate: LoginViewModelDelegate!
    weak var authentication: AuthenticationDelegate?

    let loginRenderable = InputView.Renderable(type: .email, title: Localized.LoginView.TextTitle.emailTitle, placeholder: Localized.LoginView.Placeholder.emailPlaceholder, isSecure: false, hint: nil)
    let passwordRenderable = InputView.Renderable(type: .password, title: Localized.LoginView.TextTitle.passwordTitle , placeholder: Localized.LoginView.Placeholder.passwordPlaceholder , isSecure: true, hint: Localized.LoginView.TextTitle.hintPasswordTitle)
    
    private let inputsRequired: Set<InputView.InputType> = [.email, .password]
    private var inputs: Set<InputView.InputType> = []
}

extension LoginViewModel: LoginViewModelProtocol {    
    var loginButtonTitle: String { Localized.LoginView.Button.loginButtonTitle }
    
    func isInputEmpty(type: InputView.InputType, isEmpty: Bool) {
        if isEmpty {
            inputs.remove(type)
        } else {
            inputs.insert(type)
        }
        //delegate.updateButton(isEnabled: inputs == inputsRequired)
        if inputs == inputsRequired {
            delegate.updateForm()
        }
    }
    
    func login(email: String?, password: String?) {
//        delegate.updateButton(isEnabled: false)
        delegate.updateForm()
        guard let email = email,
              let password = password else { return }
        
        AuthService.logUserIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print("DEBUG: Failed to log user in \(error.localizedDescription)")
                return
            }
            self.authentication?.authenticationDidComplete()
        }
     }
    
    func showSignUpController() {
        print("DEBUG: SHOW SIGN UP CONTROLLER - not working")
        let viewModel = RegistrationViewModel()
        viewModel.authenticationdelegate = authentication
        let controller = RegistrationViewController(with: viewModel)
        let nav = UINavigationController()
        nav.pushViewController(controller, animated: true)
    }
    
}
