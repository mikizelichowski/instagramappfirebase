//
//  LoginViewController.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 23/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {
    private var viewModelAuthentication = LoginAuthenticationViewModel()
    var viewModel: LoginViewModelProtocol
    
    init(with viewModel: LoginViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let iconImage: UIImageView = {
        let iv = UIImageView()
        iv.image = Asset.instagramImage.image
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    private let loginInput = InputView()
    private let passwordInput = InputView()
    private let containerStackView = UIStackView()
    private var loginButton: RoundedButton = {
        let button = RoundedButton()
        button.addTarget(self, action: #selector(handleLoginDidTap), for: .touchUpInside)
        return button
    }()
    
    private let forgotPasswordButton: UIButton = {
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: Localized.LoginView.TextTitle.forgotPassword, secondPart: Localized.LoginView.Button.getPassword)
        button.addTarget(self, action: #selector(handleTapped), for: .touchUpInside)
        return button
    }()
    
    private let dontHaveAccountButton: UIButton = {
        let button = UIButton(type: .system)
        button.attributedTitle(firstPart: Localized.LoginView.TextTitle.dontHaveAccount, secondPart: Localized.LoginView.Button.signIn)
        button.addTarget(self, action: #selector(handleShowSignUp), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        //configureNotificationObservers()
    }
    
    private func configureUI() {
        configureGradientLayer()
        view.addSubview(iconImage)
        iconImage.centerX(inView: view)
        iconImage.setDimensions(height: 80,
                                width: 120)
        iconImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 32)
        loginButton.setup(title: viewModel.loginButtonTitle, style: .login)
        
        loginInput.update(renderable: viewModel.loginRenderable)
        loginInput.isEmptyClosure = viewModel.isInputEmpty
        passwordInput.update(renderable: viewModel.passwordRenderable)
        passwordInput.isEmptyClosure = viewModel.isInputEmpty
        
        [loginInput, passwordInput].forEach { containerStackView.addArrangedSubview($0)}
        containerStackView.axis = .vertical
        containerStackView.spacing = 10
        containerStackView.distribution = .fillEqually
        view.addSubview(containerStackView)
        containerStackView.setHeight(150)
        containerStackView.anchor(top: iconImage.bottomAnchor,
                                  left: view.leftAnchor,
                                  right: view.rightAnchor,
                                  paddingTop: 32,
                                  paddingLeft: 32,
                                  paddingRight: 32)
        let stack = UIStackView(arrangedSubviews: [loginButton, forgotPasswordButton])
        stack.axis = .vertical
        stack.spacing = 5
        stack.distribution = .fillEqually
        view.addSubview(stack)
        stack.setHeight(100)
        stack.anchor(top: containerStackView.bottomAnchor,
                     left: view.leftAnchor,
                     right: view.rightAnchor,
                     paddingTop: 20,
                     paddingLeft: 32,
                     paddingRight: 32)
        
        
        view.addSubview(dontHaveAccountButton)
        dontHaveAccountButton.centerX(inView: view)
        dontHaveAccountButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        setupNavigation(true)
    }
    
    @objc private func handleShowSignUp() {
        let viewModel = RegistrationViewModel()
        let controller = RegistrationViewController(with: viewModel)
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc private func textDidChange(sender: UITextField) {
        sender == loginInput ? (viewModelAuthentication.email = sender.text) : (viewModelAuthentication.password = sender.text)
        updateForm()
    }
    
    @objc
    private func handleLoginDidTap() {
        viewModel.login(email: loginInput.text, password: passwordInput.text)
    }
    
    @objc
    private func handleTapped() {
        let controller = ResetPasswordController()
        controller.delegate = self
        controller.email = loginInput.text
        navigationController?.pushViewController(controller, animated: true)
    }
    
    #warning("do poprawienia !!")
    //    private func configureNotificationObservers() {
    //    emailTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    //    passwordTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    //    }
}

extension LoginViewController: LoginViewModelDelegate {
    //    func updateButton(isEnabled: Bool) {}
    
    func updateForm() {
        
        loginButton.backgroundColor = viewModelAuthentication.buttonBackgroundColor
        loginButton.setTitleColor(viewModelAuthentication.buttonTitleColor, for: .normal)
        loginButton.isEnabled = viewModelAuthentication.formIsValid
    }
}

extension LoginViewController: ResetPasswordControllerDelegate {
    func controllerDidSendResetPasswordLink(_ controller: ResetPasswordController) {
        navigationController?.popViewController(animated: true)
        showMessage(withTitle: "Success", message: "We sent a link to your email to reset your password")
    }
}
