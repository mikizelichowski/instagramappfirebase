//
//  ResetPassword.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 14/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol FormViewModel {
    func updateForm()
}

protocol ResetPasswordControllerDelegate: class {
    func controllerDidSendResetPasswordLink(_ controller: ResetPasswordController)
}

final class ResetPasswordController: UIViewController {
    private enum Constants {
        static let magicNumberThirtyTwo: CGFloat = 32.0
        static let magicNumberHundertTwo: CGFloat = 120.0
    }
    
    private let iconImage = UIImageView(image: #imageLiteral(resourceName: "Instagram_logo_white"))
    private let emailTextFiled = CustomTextField(placeholder: "Email")
    private let resetPasswordButton = CustomButton(style: .customButton)
    private let backButton = UIButton()
    var email: String?   
    
    private var viewModel = ResetPasswordViewModel()
    weak var delegate: ResetPasswordControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        setupLabel()
    }
    
    private func configureUI() {
        
        configureGradientLayer()
        [iconImage, backButton].forEach { view.addSubview($0) }
        backButton.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                          left: view.safeAreaLayoutGuide.leftAnchor,
                          paddingTop: CGFloat(StringRepresentationOfDigit.sixteen),
                          paddingLeft: CGFloat(StringRepresentationOfDigit.sixteen),
                          width: CGFloat(StringRepresentationOfDigit.eigthy),
                          height: CGFloat(StringRepresentationOfDigit.eigthy))
        iconImage.centerX(inView: view)
        iconImage.setDimensions(height: CGFloat(StringRepresentationOfDigit.eight),
                                width: Constants.magicNumberHundertTwo)
        iconImage.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         paddingTop: Constants.magicNumberThirtyTwo)
        
        let stack = UIStackView(arrangedSubviews: [emailTextFiled, resetPasswordButton])
        stack.axis = .vertical
        stack.spacing = CGFloat(StringRepresentationOfDigit.twenty)
        view.addSubview(stack)
        stack.anchor(top: iconImage.bottomAnchor,
                     left: view.safeAreaLayoutGuide.leftAnchor,
                     right: view.rightAnchor,
                     paddingTop: Constants.magicNumberThirtyTwo,
                     paddingLeft: CGFloat(StringRepresentationOfDigit.sixteen),
                     paddingRight: CGFloat(StringRepresentationOfDigit.sixteen))
    }
    
    private func setupLabel() {
        
        emailTextFiled.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        emailTextFiled.text = email
        viewModel.email = email
        updateForm()
        
        resetPasswordButton.setTitle("Reset Password", for: .normal)
        resetPasswordButton.setTitleColor(.white, for: .normal)
        resetPasswordButton.addTarget(self, action: #selector(handleTappedResetButton), for: .touchUpInside)
        
        backButton.addTarget(self, action: #selector(handleTappedBackButton), for: .touchUpInside)
        backButton.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        backButton.tintColor = .white
        iconImage.contentMode = .scaleAspectFill
    }
    
    @objc
    private func handleTappedResetButton() {
        guard let email = emailTextFiled.text else { return }
        showLoader(true)
        AuthService.resetPassword(withEmail: email) { error in
            if let error = error {
                self.showMessage(withTitle: "Error", message: error.localizedDescription)
                self.showLoader(false)
                return
            }
            self.delegate?.controllerDidSendResetPasswordLink(self)
        }
    }
    
    @objc
    private func handleTappedBackButton() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc
    func textDidChange(sender: UITextField) {
        if sender == emailTextFiled {
            viewModel.email = sender.text
        }
        updateForm()
    }
}

extension ResetPasswordController: FormViewModel {
    func updateForm() {
        resetPasswordButton.backgroundColor = viewModel.buttonBackgroundColor
        resetPasswordButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        resetPasswordButton.isEnabled = viewModel.formIsValid
    }
}
