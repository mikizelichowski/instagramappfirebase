//
//  MagicNumbers.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

enum StringRepresentationOfDigit {
    static let zero = 0
    static let one = 1
    static let two = 2
    static let three = 3
    static let four = 4
    static let five = 5
    static let six = 6
    static let eight = 8
    static let nine = 9
    static let ten = 10
    static let twelve = 12
    static let sixteen = 16
    static let twenty = 20
    static let fourty = 40
    static let hour = 60
    static let eigthy = 80
    static let hundred = 100
}
