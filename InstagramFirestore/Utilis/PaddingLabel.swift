//
//  PaddingLabel.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 09/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

@IBDesignable class PaddingLabel: UILabel {
    private enum Constants {
        static let cornerRadius: CGFloat = 5.0
    }
    
    @IBInspectable var topInset: CGFloat = 2.0
    @IBInspectable var bottomInset: CGFloat = 2.0
    @IBInspectable var leftInset: CGFloat = 4.0
    @IBInspectable var rightInset: CGFloat = 4.0
    
    init() {
        super.init(frame: .zero)
        
        layer.masksToBounds = true
        layer.cornerRadius = Constants.cornerRadius
    }
    
    override func drawText(in rect: CGRect) {
        let inset = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: inset))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset, height: size.height + topInset + bottomInset)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
