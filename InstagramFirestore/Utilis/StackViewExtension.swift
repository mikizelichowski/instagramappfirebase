//
//  StackViewExtension.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 02/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

extension UIStackView {
    func removeAll() {
        self.arrangedSubviews.forEach( { (view) in
            view.removeFromSuperview()
        })
    }
    
    func addArrangedSubviews(_ views: UIView...) {
        views.forEach { self.addSubview($0)}
    }
}
