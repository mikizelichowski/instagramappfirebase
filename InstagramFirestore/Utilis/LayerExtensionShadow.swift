//
//  LayerExtensionShadow.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 02/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

extension CALayer {
    enum ShadowType {
        case profileButton
        case cell
        case loginContent
        case alert
        case overlayContainer
    }
    
    func addShadow(type: ShadowType) {
        switch type {
        case .profileButton:
            shadowOffset = CGSize(width: .zero, height: 8.0)
            shadowOpacity = 2.0
            shadowRadius = 4.0
            shadowColor = UIColor.dashboardTicketsViewCellShadow.cgColor
        case .cell:
            cornerRadius = 4.0
            shadowOffset = CGSize(width: 0.0, height: 4.0)
            shadowColor = UIColor.shadow.cgColor
            shadowOpacity = 1.0
            shadowRadius = 10.0
        case .loginContent:
            cornerRadius = 4.0
            shadowOffset = CGSize(width: 0.0, height: 2.0)
            shadowColor = UIColor.shadow.cgColor
            shadowOpacity = 1.0
            shadowRadius = 16.0
        case .alert:
            cornerRadius = 4.0
            shadowOffset = CGSize(width: 0.0, height: 4.0)
            shadowColor = UIColor.shadow.cgColor
            shadowOpacity = 1.0
            shadowRadius = 10.0
        case .overlayContainer:
            masksToBounds = false
            cornerRadius = 4.0
            shadowOffset = CGSize(width: 0.0, height: -11.0)
            shadowColor = UIColor.dashboardTicketsViewCellShadow.cgColor
            shadowOpacity = 0.5
            shadowRadius = 8.0
            maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
    }
}
