//
//  UIViewControllerExtension.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 23/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit
import JGProgressHUD

extension UIViewController {
    static let hud = JGProgressHUD(style: .dark)
    
    func configureGradientLayer() {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.systemPurple.cgColor, UIColor.systemBlue.cgColor]
        gradient.locations = [0,1]
        view.layer.addSublayer(gradient)
        gradient.frame = view.frame
    }
    
    func showLoader(_ show: Bool) {
        view.endEditing(true)
        if show {
            UIViewController.hud.show(in: view)
        } else {
            UIViewController.hud.dismiss()
        }
    }
    
    func showMessage(withTitle title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func setupNavigation(_ hide: Bool, title: String? = nil) {
        navigationController?.navigationBar.isHidden = hide
        navigationItem.title = title
    }
    
    func setunNavigationLargeTitle(_ hide: Bool, largeTitle: Bool, title: String?, colorTitle: UIColor?, backgroundColor: UIColor?) {
        setupNavigation(hide, title: title)
        navigationController?.navigationBar.prefersLargeTitles = largeTitle
        navigationController?.navigationBar.tintColor = colorTitle
        navigationController?.navigationBar.backgroundColor = backgroundColor
    }
    
    func logoutAlert(title: String? = nil, message: String? = nil, handler: ((UIAlertAction) -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Localized.FeedView.Alert.logOut, style: .destructive, handler: handler))
        alert.addAction(UIAlertAction(title: Localized.FeedView.Alert.cancel, style: .cancel, handler: handler))
        self.present(alert, animated: true, completion: nil)
    }
}
