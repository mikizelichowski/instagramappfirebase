//
//  StringExtension.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 25/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

extension String {
    static let empty = ""
    static let space = " "
}
