//
//  CollectionExtension.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

enum CollectionItemsCount: Int {
    case oneElement = 1
    case twoItems, trheeItems, fourItems, fiveItems, sixItems, sevenItems, eightItems, nineItems, tenItems
}

extension Collection {
    var isNotEmpty: Bool {
        return !isEmpty
    }
    
    func has(just items: CollectionItemsCount) -> Bool {
        return count == items.rawValue
    }
}
