//
//  UICollectionViewExtension.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 02/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

extension UICollectionView {
    func registerCell<T: UICollectionViewCell>(_ cellClass: T.Type) {
        register(cellClass, forCellWithReuseIdentifier: cellClass.identifier)
    }
        
    func dequeueReusableCell<T: UICollectionViewCell>(indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as! T
    }
}
