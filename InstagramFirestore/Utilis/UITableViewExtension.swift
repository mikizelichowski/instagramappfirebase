//
//  UITableViewExtension.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 09/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

extension UITableView {
    func registerTableViewCell<T: UITableViewCell>(_ cellClass: T.Type) {
        register(cellClass, forCellReuseIdentifier: cellClass.identifier)
    }
    
    func dequeueReusableTablewViewCell<T: UITableViewCell>(indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as! T
    }
}
