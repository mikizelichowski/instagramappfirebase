//
//  Identifiable.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 02/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

protocol Identifiable {
    static var identifier: String { get }
}

extension Identifiable {
    static var identifier: String {
        return String(describing: self)
    }
}

extension UIView: Identifiable {}
