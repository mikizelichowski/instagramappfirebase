//
//  UIColorExtension.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 23/10/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

extension UIColor {
    static let whiteAlphaTf = UIColor(white: 1, alpha: 0.67)
    static let redColor = UIColor(red: 0.75, green: 0, blue: 0.02, alpha: 1.0)
    static let blueDark = UIColor(red: 0.02, green: 0.23, blue: 0.40, alpha: 1.0)
    static let greyBlueLight = UIColor(red: 0.4, green: 0.47, blue: 0.54, alpha: 1.0)
    static let shadow = UIColor(red: 0, green: 0.18, blue: 0.35, alpha: 0.1)
    static let dashboardTicketsViewCellShadow = UIColor(red: 0.12, green: 0.36, blue: 0.61, alpha: 0.3)
    static let lineConnecting = UIColor(red: 0.45, green: 0.55, blue: 0.66, alpha: 0.5)
}
