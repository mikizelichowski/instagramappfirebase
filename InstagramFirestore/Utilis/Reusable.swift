//
//  Reusable.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 04/12/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

protocol Reusable: AnyObject {
    static var reuseIdenfitier: String { get }
}

extension Reusable {
    static var reuseIdentifier: String {
        String(reflecting: self)
    }
}
