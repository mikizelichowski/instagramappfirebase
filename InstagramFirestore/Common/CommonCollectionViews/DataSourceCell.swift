//
//  DataSourceCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation
import UIKit.UICollectionViewCell

class DataSourceCell<T: DataSourceIdentifiable & Equatable & Hashable>: UICollectionViewCell {
    var model: T! {
        didSet {
            update()
        }
    }
    
    func update() { }
}
