//
//  CommonDataSourceItem.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 06/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

struct CommonViewDatasourceItem: DataSourceIdentifiable & Equatable & Hashable {
    let identity: Int
    let viewIdentifier: String?
    let title: String
    let imageView: UIImage? = nil
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(viewIdentifier)
    }
    
    static func == (lhs: CommonViewDatasourceItem, rhs: CommonViewDatasourceItem) -> Bool {
        return lhs.identity == rhs.identity
    }
}
