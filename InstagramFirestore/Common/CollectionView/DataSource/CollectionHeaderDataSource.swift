//
//  CollectionHeaderDataSource.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 04/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation
import UIKit.UICollectionViewCell

class CollectionHeaderDataSource<T: DataSourceIdentifiable & Equatable & Hashable>: UICollectionReusableView {
    
//class CollectionHeaderDataSource<T: DataSourceSection>: UICollectionReusableView {
    var model: T! {
        didSet {
            update()
        }
    }
    
    func update() {
        
    }
}
