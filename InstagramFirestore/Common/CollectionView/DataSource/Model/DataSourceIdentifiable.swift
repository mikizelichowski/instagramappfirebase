//
//  DataSourceIdentifiable.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 04/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

protocol DataSourceIdentifiable: Equatable {
    associatedtype Identity: Hashable
    
    var identity: Identity { get }
    var viewIdentifier: String? { get }
    
}
