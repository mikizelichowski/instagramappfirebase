//
//  DataSourceSection.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 04/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

protocol DataSourceSection: DataSourceIdentifiable {
    associatedtype Item: DataSourceIdentifiable & Equatable & Hashable
    
    var items: [Item] { get }
    var title: String? { get }
}
