//
//  CollectionDataSourceSectionView.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 05/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation
import UIKit

class CollectionDataSourceSectionView<T: DataSourceSection>: UICollectionView {
    var mode: T! {
        didSet {
            update()
        }
    }
    
    func update() {
        
    }
}
