//
//  CommonDataSourceItem.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 07/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import UIKit

struct TableViewDataSourceItem: DataSourceIdentifiable & Equatable & Hashable {
    let identity: Int
    let viewIdentifier: String?
    let title: String
    let isNotSelected: Bool
    var subtitle: String? = nil
    var isFavorite: Bool = false
    var image: UIImage? = nil
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(viewIdentifier)
    }
    
    static func == (lhs: TableViewDataSourceItem, rhs:  TableViewDataSourceItem) -> Bool {
        return lhs.identity == rhs.identity
    }
}
