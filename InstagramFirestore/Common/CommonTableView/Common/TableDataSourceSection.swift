//
//  CommonDataSourceSection.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 07/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation

struct TableViewDataSourceSection: DataSourceSection {
    let identity: String
    let viewIdentifier: String?
    let items: [TableViewDataSourceItem]
    let title: String?
}

enum TableViewSectionStyle: CaseIterable {
    case sectionOne
    case sectionTwo
    case sectionThree
    case sectionFour
}
