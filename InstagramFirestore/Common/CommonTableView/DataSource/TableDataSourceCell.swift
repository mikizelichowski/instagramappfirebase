//
//  DataSourceCell.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 08/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation
import UIKit.UITableViewCell

class TableDataSourceCell<T: DataSourceIdentifiable & Equatable & Hashable>: UITableViewCell {
    var model: T! {
        didSet { update()}
    }
    
    func update() { }
}
