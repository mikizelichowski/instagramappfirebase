//
//  DataSourceSectionView.swift
//  InstagramFirestore
//
//  Created by Mikolaj Zelichowski on 08/11/2020.
//  Copyright © 2020 Infusion Code. All rights reserved.
//

import Foundation
import UIKit

class DataSourceSectionView<T: DataSourceSection>: UITableViewHeaderFooterView {
    var model: T! {
        didSet { update() }
    }
    
    func update() { }
}
